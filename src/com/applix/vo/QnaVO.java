package com.applix.vo;

import java.sql.Timestamp;

public class QnaVO {

	private int qseq;
	private String subject;
	private String content;
	private String reply;
	private String replyyn;
	private String qname;
	private Timestamp indate;
	private String indate2;
	private String id;

	public String getIndate2() {
		return indate2;
	}

	public void setIndate2(String indate2) {
		this.indate2 = indate2;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getIndate() {
		return indate;
	}

	public void setIndate(Timestamp indate) {
		this.indate = indate;
	}

	public int getQseq() {
		return qseq;
	}

	public void setQseq(int qseq) {
		this.qseq = qseq;
	}

	public String getQname() {
		return qname;
	}

	public void setQname(String qname) {
		this.qname = qname;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public String getReplyyn() {
		return replyyn;
	}

	public void setReplyyn(String replyyn) {
		this.replyyn = replyyn;
	}

}
