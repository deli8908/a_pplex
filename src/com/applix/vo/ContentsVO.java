package com.applix.vo;

public class ContentsVO {

	private int movieNum;
	private String title;
	private int runningTime;
	private String age;
	private String director;
	private String char1;
	private String char2;
	private String char3;
	private String genre;
	private String synop;
	private String releaseDate;
	private String img;
	private String active;
	private int price1;
	private int price2;
	private String saleyn;
	private String newyn;
	private String bestyn;
	private String recoyn;
	private String iFrame;
	

	public String getNewyn() {
		return newyn;
	}

	public void setNewyn(String newyn) {
		this.newyn = newyn;
	}

	public String getBestyn() {
		return bestyn;
	}

	public void setBestyn(String bestyn) {
		this.bestyn = bestyn;
	}

	public String getRecoyn() {
		return recoyn;
	}

	public void setRecoyn(String recoyn) {
		this.recoyn = recoyn;
	}


	public int getMovieNum() {
		return movieNum;
	}

	public void setMovieNum(int movieNum) {
		this.movieNum = movieNum;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getRunningTime() {
		return runningTime;
	}

	public void setRunningTime(int runningTime) {
		this.runningTime = runningTime;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getChar1() {
		return char1;
	}

	public void setChar1(String char1) {
		this.char1 = char1;
	}

	public String getChar2() {
		return char2;
	}

	public void setChar2(String char2) {
		this.char2 = char2;
	}

	public String getChar3() {
		return char3;
	}

	public void setChar3(String char3) {
		this.char3 = char3;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getSynop() {
		return synop;
	}

	public void setSynop(String synop) {
		this.synop = synop;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getPrice1() {
		return price1;
	}

	public void setPrice1(int price1) {
		this.price1 = price1;
	}

	public int getPrice2() {
		return price2;
	}

	public void setPrice2(int price2) {
		this.price2 = price2;
	}

	public String getSaleyn() {
		return saleyn;
	}

	public void setSaleyn(String saleyn) {
		this.saleyn = saleyn;
	}

	public String getiFrame() {
		return iFrame;
	}

	public void setiFrame(String iFrame) {
		this.iFrame = iFrame;
	}

}
