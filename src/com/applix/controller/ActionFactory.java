package com.applix.controller;

import com.applix.controller.action.Action;
import com.applix.controller.action.AdminBoardAction;
import com.applix.controller.action.AdminBoardDeleteAction;
import com.applix.controller.action.AdminBoardDetailAction;
import com.applix.controller.action.AdminInsertAction;
import com.applix.controller.action.AdminInsertFormAction;
import com.applix.controller.action.AdminInsertQnaReplyAction;
import com.applix.controller.action.AdminLoginAction;
import com.applix.controller.action.AdminLoginFormAction;
import com.applix.controller.action.AdminLogoutAction;
import com.applix.controller.action.AdminMainAction;
import com.applix.controller.action.AdminMemberAction;
import com.applix.controller.action.AdminOrderAction;
import com.applix.controller.action.AdminQnaAction;
import com.applix.controller.action.AdminQnaDetailAction;
import com.applix.controller.action.AdminReviewAction;
import com.applix.controller.action.AdminReviewDeleteAction;
import com.applix.controller.action.AdminReviewDetailAction;
import com.applix.controller.action.AdminSearchBoardAction;
import com.applix.controller.action.AdminSearchOrderAction;
import com.applix.controller.action.AdminUpdateAction;
import com.applix.controller.action.AdminUpdateFormAction;
import com.applix.controller.action.AlltheContentsAction;
import com.applix.controller.action.BuyContentsAction;
import com.applix.controller.action.CartDeleteAction;
import com.applix.controller.action.CartInsertAction;
import com.applix.controller.action.CartListAction;
import com.applix.controller.action.ChangePwAction;
import com.applix.controller.action.ChangePwFormAction;
import com.applix.controller.action.ContentsDetailAction;
import com.applix.controller.action.DeletePostAction;
import com.applix.controller.action.DeleteReplyAction;
import com.applix.controller.action.FreeBoardAction;
import com.applix.controller.action.FreeBoardDetailAction;
import com.applix.controller.action.FreeBoardReplyInsertAction;
import com.applix.controller.action.FreeBoardWriteAction;
import com.applix.controller.action.FreeBoardWriteFormAction;
import com.applix.controller.action.IndexAction;
import com.applix.controller.action.JoinusAction;
import com.applix.controller.action.JoinusFormAction;
import com.applix.controller.action.LoginAction;
import com.applix.controller.action.LoginFormAction;
import com.applix.controller.action.LogoutAction;
import com.applix.controller.action.MovieDeleteAction;
import com.applix.controller.action.MyBoardAction;
import com.applix.controller.action.MyPageAction;
import com.applix.controller.action.MyPageUpdateAction;
import com.applix.controller.action.MyReviewAction;
import com.applix.controller.action.OrderListAction;
import com.applix.controller.action.QnaDetailListAction;
import com.applix.controller.action.QnaListAction;
import com.applix.controller.action.QnaWriteAction;
import com.applix.controller.action.QnaWriteFormAction;
import com.applix.controller.action.RemoveAccountAction;
import com.applix.controller.action.ReviewAction;
import com.applix.controller.action.ReviewDetailAction;
import com.applix.controller.action.ReviewReplyInsertAction;
import com.applix.controller.action.ReviewWriteAction;
import com.applix.controller.action.ReviewWriteFormAction;
import com.applix.controller.action.SearchMemberAction;
import com.applix.controller.action.SearchforContentsAction;
import com.applix.controller.action.SelectReviewAction;
import com.applix.controller.action.WatchAction;

public class ActionFactory {
	private static ActionFactory instance = new ActionFactory();

	public static ActionFactory getInstance() {
		return instance;
	}

	public Action getAction(String command) {
		Action action = null;
		if (command.equals("index")) {
			action = new IndexAction();
		} else if (command.equals("contents_detail")) {
			action = new ContentsDetailAction();
		} else if (command.equals("login_form")) {
			action = new LoginFormAction();
		} else if (command.equals("login")) {
			action = new LoginAction();
		} else if (command.equals("logout")) {
			action = new LogoutAction();
		} else if (command.equals("joinusform")) {
			action = new JoinusFormAction();
		} else if (command.equals("joinus")) {
			action = new JoinusAction();
		} else if (command.equals("mypage")) {
			action = new MyPageAction();
		} else if (command.equals("update_mypage")) {
			action = new MyPageUpdateAction();
		} else if (command.equals("change_password")) {
			action = new ChangePwAction();
		} else if (command.equals("change_password_form")) {
			action = new ChangePwFormAction();
		} else if (command.equals("myreview")) {
			action = new MyReviewAction();
		} else if (command.equals("myboard")) {
			action = new MyBoardAction();
		} else if (command.equals("review")) {
			action = new ReviewAction();
		} else if (command.equals("review_detail")) {
			action = new ReviewDetailAction();
		} else if (command.equals("review_write")) {
			action = new ReviewWriteAction();
		} else if (command.equals("review_reply_insert")) {
			action = new ReviewReplyInsertAction();
		} else if (command.equals("write_form_review")) {
			action = new ReviewWriteFormAction();
		} else if (command.equals("write_form_freeboard")) {
			action = new FreeBoardWriteFormAction();
		} else if (command.equals("free_board")) {
			action = new FreeBoardAction();
		} else if (command.equals("freeboard_detail")) {
			action = new FreeBoardDetailAction();
		} else if (command.equals("board_write")) {
			action = new FreeBoardWriteAction();
		} else if (command.equals("freeboard_insert")) {
			action = new FreeBoardReplyInsertAction();
		} else if (command.equals("qnaList")) {
			action = new QnaListAction();
		} else if (command.equals("write_qna_form")) {
			action = new QnaWriteFormAction();
		} else if (command.equals("write_qna")) {
			action = new QnaWriteAction();
		} else if (command.equals("qna_detail_list")) {
			action = new QnaDetailListAction();
		} else if (command.equals("admin_login_form")) {
			action = new AdminLoginFormAction();
		} else if (command.equals("admin_login")) {
			action = new AdminLoginAction();
		} else if (command.equals("admin_logout")) {
			action = new AdminLogoutAction();
		} else if (command.equals("admin_main")) {
			action = new AdminMainAction();
		} else if (command.equals("admin_insert")) {
			action = new AdminInsertAction();
		} else if (command.equals("admin_insert_contents")) {
			action = new AdminInsertFormAction();
		} else if (command.equals("admin_member")) {
			action = new AdminMemberAction();
		} else if (command.equals("update_form")) {
			action = new AdminUpdateFormAction();
		} else if (command.equals("admin_update")) {
			action = new AdminUpdateAction();
		} else if (command.equals("remove_account")) {
			action = new RemoveAccountAction();
		} else if (command.equals("cart_insert")) {
			action = new CartInsertAction();
		} else if (command.equals("cart_list")) {
			action = new CartListAction();
		} else if (command.equals("cart_delete")) {
			action = new CartDeleteAction();
		} else if (command.equals("orderlist")) {
			action = new OrderListAction();
		} else if (command.equals("selected_review")) {
			action = new SelectReviewAction();
		} else if (command.equals("allthelist")) {
			action = new AlltheContentsAction();
		} else if (command.equals("search_contents")) {
			action = new SearchforContentsAction();
		} else if (command.equals("delete_post")) {
			action = new DeletePostAction();
		} else if (command.equals("delete_reply")) {
			action = new DeleteReplyAction();
		} else if (command.equals("search_member")) {
			action = new SearchMemberAction();
		} else if (command.equals("buy_contents")) {
			action = new BuyContentsAction();
		} else if (command.equals("watch")) {
			action = new WatchAction();
		} else if (command.equals("movie_delete")) {
			action = new MovieDeleteAction();
		} else if (command.equals("admin_review")) {
			action = new AdminReviewAction();
		} else if (command.equals("admin_review_detail")) {
			action = new AdminReviewDetailAction();
		} else if (command.equals("admin_review_delete")) {
			action = new AdminReviewDeleteAction();
		} else if (command.equals("admin_freeboard")) {
			action = new AdminBoardAction();
		} else if (command.equals("admin_board_detail")) {
			action = new AdminBoardDetailAction();
		} else if (command.equals("admin_board_delete")) {
			action = new AdminBoardDeleteAction();
		} else if (command.equals("admin_order")) {
			action = new AdminOrderAction();
		} else if (command.equals("search_orderlist")) {
			action = new AdminSearchOrderAction();
		} else if (command.equals("search_board")) {
			action = new AdminSearchBoardAction();
		} else if (command.equals("admin_qna")) {
			action = new AdminQnaAction();
		} else if (command.equals("admin_qna_detail")) {
			action = new AdminQnaDetailAction();
		} else if (command.equals("insert_qna_reply")) {
			action = new AdminInsertQnaReplyAction();
		}

		return action;
	}
}
