package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;

public class MyPageUpdateAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "ApplixServlet?command=mypage";

		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			MemberDAO memberDAO = MemberDAO.getInstance();
			String new_email = request.getParameter("email");
			String new_phone = request.getParameter("phone");

			MemberVO member = new MemberVO();
			member.setEmail(new_email);
			member.setPhone(new_phone);
			member.setId(loginUser.getId());

			memberDAO.update_inform(member);
			request.getRequestDispatcher(url).forward(request, response);
		}
	}
}
