package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReplyVO;
import com.applix.vo.ReviewVO;

public class DeletePostAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			String[] fseqArr = request.getParameterValues("fseq");
			String[] rseqArr = request.getParameterValues("rseq");

			if (fseqArr != null) {
				for (String fseq : fseqArr) {
					BoardDAO board = BoardDAO.getInstance();
					board.deletePost(Integer.parseInt(fseq), 2);
					url = "ApplixServlet?command=myboard";
				}
			} else {
				for (String rseq : rseqArr) {
					BoardDAO board = BoardDAO.getInstance();
					board.deletePost(Integer.parseInt(rseq), 1);
					url = "ApplixServlet?command=myreview";
				}
			}
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
