package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReviewVO;

public class AdminReviewAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = "admin/admin_review.jsp";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			BoardDAO board = BoardDAO.getInstance();
			ArrayList<ReviewVO> reviewList = board.getAllReviews();
			
			request.setAttribute("reviewList", reviewList);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
