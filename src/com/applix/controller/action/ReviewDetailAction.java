package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReplyVO;
import com.applix.vo.ReviewVO;

public class ReviewDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "board/reviewDetail.jsp";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			try {
				int rseq = Integer.parseInt(request.getParameter("rseq"));
				BoardDAO boardDAO = BoardDAO.getInstance();
				ReviewVO detailList = boardDAO.reviewDetail(rseq);
				ArrayList<ReplyVO> datList = boardDAO.boardDat(rseq, 1);
				
				request.setAttribute("detailList", detailList);
				request.setAttribute("datList", datList);
				request.setAttribute("loginUser", loginUser);
				request.setAttribute("rseq", rseq);
				
//				System.out.println("review detail : " +rseq);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
