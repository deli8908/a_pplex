package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.FreeBoardVO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReviewVO;

public class FreeBoardWriteFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String url = "mypage/writeFreeBoard.jsp";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO)session.getAttribute("loginUser");
		if(loginUser == null) {
			url = "ApplixServlet?command=login_form";
		}else {
			BoardDAO boardDAO = BoardDAO.getInstance();
			String from = request.getParameter("from");
			ArrayList<FreeBoardVO> freebdList = boardDAO.getAllBoard();
			request.setAttribute("from", from);
			request.setAttribute("freeboard", freebdList);
			request.setAttribute("loginUser", loginUser);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
