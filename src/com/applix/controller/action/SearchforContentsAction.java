package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;
import com.applix.vo.MemberVO;

public class SearchforContentsAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "contents/allthelist.jsp";

		String key = "";
		String select = "";
		if (request.getParameter("key") != null) {
			key = request.getParameter("key");
			select = request.getParameter("select");
		}

		ContentsDAO conDAO = ContentsDAO.getInstance();
		ArrayList<ContentsVO> all = conDAO.searchList(select, key);

		request.setAttribute("all", all);
		request.getRequestDispatcher(url).forward(request, response);
	}
}
