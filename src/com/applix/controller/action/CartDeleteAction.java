package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.CartDAO;
import com.applix.dao.OrderDAO;
import com.applix.vo.CartVO;
import com.applix.vo.MemberVO;

public class CartDeleteAction implements Action {

	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String url = "ApplixServlet?command=cart_list";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			OrderDAO orderDAO = OrderDAO.getInstance();

			String cseqs[] = request.getParameterValues("cseq");
			CartDAO cart = CartDAO.getInstance();

			for (String csnum : cseqs) {
				int cseq = Integer.parseInt(csnum);
				ArrayList<CartVO> cartList = cart.cartselectedList(loginUser.getId(), cseq);
				orderDAO.insertOrder(cartList, 2);
			}
		}
		response.sendRedirect(url);
	}
}