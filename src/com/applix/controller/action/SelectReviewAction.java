package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReviewVO;

public class SelectReviewAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "board/selectedReview.jsp";

		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			BoardDAO board = BoardDAO.getInstance();
			String title = request.getParameter("title");
			ArrayList<ReviewVO> review = board.selectedReview(title);
			
			ContentsDAO conDAO = ContentsDAO.getInstance();
			int num = conDAO.findnum(title);
			
			request.setAttribute("num", num);
			request.setAttribute("title", title);
			request.setAttribute("review", review);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
