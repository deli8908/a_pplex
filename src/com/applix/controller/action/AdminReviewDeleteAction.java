package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.MemberVO;

public class AdminReviewDeleteAction implements Action {

	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String url = "ApplixServlet?command=admin_review";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {

			int rseq = Integer.parseInt(request.getParameter("rseq"));
			System.out.println("adminReviewDel : " + rseq);
			BoardDAO board = BoardDAO.getInstance();
			board.deletePost(rseq, 1);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}