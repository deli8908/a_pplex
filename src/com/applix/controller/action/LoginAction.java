package com.applix.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;

public class LoginAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberDAO memberDAO = MemberDAO.getInstance();
		MemberVO memberList = memberDAO.loginMember(request.getParameter("id"), request.getParameter("pw"));

		if (memberList.getId() == null) {
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8;'>");
			out.println("</head>");
			out.println("<script>");
			out.println("alert('일치하는 회원 정보가 없습니다.'); location.href='ApplixServlet?command=login_form'");
			out.println("</script>");
			out.println("/<html>");
			out.flush();
		} else {
			url = "ApplixServlet?command=index";
			session.setAttribute("loginUser", memberList);
			request.getRequestDispatcher(url).forward(request, response);
		}

//		System.out.println(memberList);

	}
}
