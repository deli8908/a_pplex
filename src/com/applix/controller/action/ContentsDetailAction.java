package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;

public class ContentsDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String url = "contents/contentsDetail.jsp";

		int num = Integer.parseInt(request.getParameter("num"));
//		System.out.println(num);
		ContentsDAO conDAO = ContentsDAO.getInstance();
		ContentsVO detailList = conDAO.contentsDetail(num);
		ArrayList<ContentsVO> resembleList = conDAO.resembleContents(detailList.getGenre(), detailList.getMovieNum());

		request.setAttribute("detailList", detailList);
		request.setAttribute("title", detailList.getTitle());
		request.setAttribute("resembleList", resembleList);
//		
//		for(ContentsVO list : resembleList) {
//			System.out.println(list);
//		}

		request.getRequestDispatcher(url).forward(request, response);
	}
}
