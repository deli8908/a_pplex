package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;
import com.applix.vo.MemberVO;

public class WatchAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "contents/watch.jsp";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			
			String title = request.getParameter("title");
			System.out.println(title);
			ContentsDAO conDAO = ContentsDAO.getInstance();
			ContentsVO detailList = conDAO.watchContents(title);

			request.setAttribute("detailList", detailList);
			request.setAttribute("title", detailList.getTitle());

			request.getRequestDispatcher(url).forward(request, response);
		}
	}
}