package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.OrderDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.OrderVO;

public class AdminSearchOrderAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			String select = request.getParameter("select");
			String key = request.getParameter("key");
			OrderDAO order = OrderDAO.getInstance();

			if (key != null) {
				ArrayList<OrderVO> orderlist = order.searchOrder(select, key);
				url = "admin/admin_orderlist.jsp";
				request.setAttribute("orderlist", orderlist);
			}
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
