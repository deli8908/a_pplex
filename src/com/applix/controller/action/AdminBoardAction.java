package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.FreeBoardVO;
import com.applix.vo.MemberVO;

public class AdminBoardAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = "admin/admin_board.jsp";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			BoardDAO board = BoardDAO.getInstance();
			ArrayList<FreeBoardVO> boardlist = board.getAllBoard();
			
			request.setAttribute("boardlist", boardlist);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
