package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.FreeBoardVO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReplyVO;

public class FreeBoardDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "board/freeboardDetail.jsp";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			try {
				int fseq = Integer.parseInt(request.getParameter("fseq"));
				BoardDAO boardDAO = BoardDAO.getInstance();
				FreeBoardVO detailList = boardDAO.boardDetail(fseq);
				ArrayList<ReplyVO> datList = boardDAO.boardDat(fseq, 2);
				
				request.setAttribute("detailList", detailList);
				request.setAttribute("datList", datList);
				request.setAttribute("loginUser", loginUser);
				request.setAttribute("fseq", fseq);
//				System.out.println("freeboard detail : " +fseq);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			request.getRequestDispatcher(url).forward(request, response);
		}
	}
}
