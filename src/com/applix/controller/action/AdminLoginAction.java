package com.applix.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;

public class AdminLoginAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "ApplixServlet?command=admin_main";
		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		String pw = request.getParameter("pw");

		MemberDAO memberDAO = MemberDAO.getInstance();
		MemberVO adminLogin = memberDAO.adminLogin(id, pw);

		if (adminLogin != null) {
			session.setAttribute("adminLogin", adminLogin);
			request.getRequestDispatcher(url).forward(request, response);
		} else {
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8;'>");
			out.println("</head>");
			out.println(
					"<script>alert('관리자만 이용 가능합니다.'); location.href='ApplixServlet?command=admin_login_form' </script>");
			out.println("</html>");
			out.flush();
		}
	}
}
