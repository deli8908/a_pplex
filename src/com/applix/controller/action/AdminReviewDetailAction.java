package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReviewVO;

public class AdminReviewDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = "admin/admin_review_detail.jsp";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			BoardDAO board = BoardDAO.getInstance();
			int rseq = Integer.parseInt(request.getParameter("rseq"));
			ReviewVO reviewList = board.reviewDetail(rseq);
			
			request.setAttribute("reviewList", reviewList);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
