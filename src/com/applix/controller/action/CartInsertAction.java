package com.applix.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.CartDAO;
import com.applix.vo.CartVO;
import com.applix.vo.MemberVO;

public class CartInsertAction implements Action {

	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String url = null;
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			int i = Integer.parseInt(request.getParameter("gocart"));
//			System.out.println("i : " + i);
			CartDAO cartDAO = CartDAO.getInstance();
			CartVO cartVO = new CartVO();
			cartVO.setId(loginUser.getId());
			cartVO.setName(loginUser.getCname());
			cartVO.setNum(Integer.parseInt(request.getParameter("num")));
			cartVO.setPrice(Integer.parseInt(request.getParameter("price")));
			cartVO.setTitle(request.getParameter("title"));

//			System.out.println("cartinsert id : " + cartVO.getId());
//			System.out.println("cartinsert num : " + cartVO.getNum());
//			System.out.println("cartinsert title : " + cartVO.getTitle());

			cartDAO.insertCart(cartVO, loginUser.getId());
			if (i == 1) {
				url = "ApplixServlet?command=cart_list";
//				System.out.println("msg 1: " + msg);
			} else {
				url = "ApplixServlet?command=contents_detail&num=" + Integer.parseInt(request.getParameter("num"));
				
//				System.out.println("msg 2: " + msg);
			}
		}
		response.sendRedirect(url);
	}
}