package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.QnaDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.QnaVO;

public class QnaWriteAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String url = "ApplixServlet?command=qnaList";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO)session.getAttribute("loginUser");
		if(loginUser == null) {
			url = "ApplixServlet?command=login_form";
		}else {
			QnaDAO qnaDAO = QnaDAO.getInstance();
			
			QnaVO qVO = new QnaVO();
			qVO.setContent(request.getParameter("content"));
			qVO.setQname(request.getParameter("name"));
			qVO.setSubject(request.getParameter("subject"));
			qVO.setId(loginUser.getId());
			
			qnaDAO.writeQna(qVO);
		}
		response.sendRedirect(url);
	}
}
