package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.CartDAO;
import com.applix.vo.CartVO;
import com.applix.vo.MemberVO;

public class CartListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "order/cartlist.jsp";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) { 
			url = "ApplixServlet?command=login_form";
		} else {
			CartDAO cartDAO = CartDAO.getInstance();
			ArrayList<CartVO> cartList = cartDAO.cartList(loginUser.getId());
			int totalPrice = 0;
			
			for(CartVO cart : cartList) {
				totalPrice += cart.getPrice();
			}
			
			request.setAttribute("totalprice", totalPrice);
			request.setAttribute("cartList", cartList);
			request.getRequestDispatcher(url).forward(request, response);
		}
	}
}
