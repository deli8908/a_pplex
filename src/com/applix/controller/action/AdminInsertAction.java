package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;
import com.applix.vo.MemberVO;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

public class AdminInsertAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = "ApplixServlet?command=admin_main";

		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			int sizeLimit = 5 * 1024 * 1024;
			int price2 = 0;
			String savePath = "/images";
			ServletContext context = session.getServletContext();
			String uploadFilePath = context.getRealPath(savePath);
			MultipartRequest multi = new MultipartRequest(request, uploadFilePath, sizeLimit, "UTF-8",
					new DefaultFileRenamePolicy());
			int price1 = Integer.parseInt(multi.getParameter("price1"));
			if (multi.getParameter("price2") == null) {
				price2 = 0;
			} else {
				price2 = Integer.parseInt(multi.getParameter("price2"));
			}
			ContentsDAO conDAO = ContentsDAO.getInstance();
			ContentsVO conVO = new ContentsVO();
			conVO.setTitle(multi.getParameter("title"));
			conVO.setRunningTime(Integer.parseInt(multi.getParameter("running")));
			conVO.setAge(multi.getParameter("age"));
			conVO.setDirector(multi.getParameter("direct"));
			conVO.setChar1(multi.getParameter("act1"));
			conVO.setChar2(multi.getParameter("act2"));
			conVO.setChar3(multi.getParameter("act3"));
			conVO.setGenre(multi.getParameter("genre"));
			conVO.setReleaseDate(multi.getParameter("indate"));
			conVO.setPrice1(price1);
			conVO.setPrice2(price2);
			conVO.setSynop(multi.getParameter("synop"));
			conVO.setiFrame(multi.getParameter("iFrame"));
			conVO.setImg(multi.getFilesystemName("image"));
			conDAO.insertContents(conVO);
		}
		response.sendRedirect(url);
	}
}
