package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;

public class RemoveAccountAction implements Action{
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "ApplixServlet?command=logout";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			MemberDAO memberDAO = MemberDAO.getInstance();
			request.setAttribute("msg", "Ż�� �Ǿ����ϴ�.");
			memberDAO.disabledMember(loginUser.getId());
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
