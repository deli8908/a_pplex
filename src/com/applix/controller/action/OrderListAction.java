package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.OrderDAO;
import com.applix.vo.CartVO;
import com.applix.vo.MemberVO;
import com.applix.vo.OrderVO;

public class OrderListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "order/orderListAll.jsp";
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) { 
			url = "ApplixServlet?command=login_form";
		} else {
			OrderDAO orderDAO = OrderDAO.getInstance();
			ArrayList<OrderVO> orderList = orderDAO.orderlist(loginUser.getId());
			
			request.setAttribute("orderList", orderList);
			request.getRequestDispatcher(url).forward(request, response);
		}
	}
}
