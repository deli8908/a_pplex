package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.MemberVO;

public class DeleteReplyAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			int seq = Integer.parseInt(request.getParameter("seq"));
			String rseq = request.getParameter("rseq");
			String fseq = request.getParameter("fseq");
			BoardDAO board = BoardDAO.getInstance();
			board.deleteReply(seq);
			if(rseq != null) {
				int rseqa = Integer.parseInt(rseq);
				url = "ApplixServlet?command=review_detail&rseq=" + rseqa;
			} else {
				int fseqa = Integer.parseInt(fseq);
				url = "ApplixServlet?command=freeboard_detail&fseq=" + fseqa;
			}
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
