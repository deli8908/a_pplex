package com.applix.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;

public class ChangePwAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = null;
		String msg = null;
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			MemberDAO memberDAO = MemberDAO.getInstance();
			MemberVO member = new MemberVO();
			String cur_pw = request.getParameter("current_pw");
			// '현재 비밀번호'창에 입력된 비밀번호
			String new_pw = request.getParameter("new_pw");
			String check_pw = request.getParameter("check_pw");
			boolean db_pw = memberDAO.checkPw(cur_pw, loginUser.getId());

			if (db_pw == true) { //db랑 입력한 비밀번호랑 동일하고
				if (new_pw.equals(check_pw)) { // 바꾸려는 비밀번호 두 개가 동일하면
					memberDAO.changePw(new_pw, loginUser.getId()); // changePw 메소드를 통해서 비밀번호를 변경한다.
					PrintWriter out = response.getWriter();
//				url ="ApplixServlet?command=mypage";
					out.println("<html>");
					out.println("<head>");
					out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8;'>");
					out.println("</head>");
					out.println("<script>");
					out.println("alert('비밀번호가 변경되었습니다.'); location.href='ApplixServlet?command=mypage'");
					out.println("</script>");
					out.println("/<html>");
					out.flush();
				} else {
					PrintWriter out = response.getWriter();
					out.println("<html>");
					out.println("<head>");
					out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8;'>");
					out.println("</head>");
					out.println("<script>");
					out.println("alert('새로운 비밀번호는 동일해야합니다.'); location.href='ApplixServlet?command=change_password_form'");
					out.println("</script>");
					out.println("/<html>");
					out.flush();
				}
			} else {
				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head>");
				out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8;'>");
				out.println("</head>");
				out.println("<script>");
				out.println("alert('비밀번호가 일치하지 않습니다.'); location.href='ApplixServlet?command=change_password_form'");
				out.println("</script>");
				out.println("/<html>");
				out.flush();
//				url = "mypage/changePw.jsp";
			}
//			System.out.println("msg : " + msg);
		}
//		request.setAttribute("msg", msg);

//		request.getRequestDispatcher(url).forward(request, response);
	}
}
