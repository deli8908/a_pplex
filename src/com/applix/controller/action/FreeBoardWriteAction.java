package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.FreeBoardVO;
import com.applix.vo.MemberVO;

public class FreeBoardWriteAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			BoardDAO boardDAO = BoardDAO.getInstance();

			FreeBoardVO freeVO = new FreeBoardVO();
			freeVO.setContent(request.getParameter("content"));
			freeVO.setFname(request.getParameter("name"));
			freeVO.setSubject(request.getParameter("subject"));
			freeVO.setId(loginUser.getId());

			boardDAO.writeBoard(freeVO);
			if (request.getParameter("from").equals("mypage")) {
				url = "ApplixServlet?command=myboard";
			} else {
				url = "ApplixServlet?command=free_board";
			}
		}
		response.sendRedirect(url);
	}
}
