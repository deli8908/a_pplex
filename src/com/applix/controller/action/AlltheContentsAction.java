package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;

public class AlltheContentsAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "contents/allthelist.jsp";
		ArrayList<ContentsVO> all = new ArrayList<ContentsVO>();
		String key = "";
		String title = null;
		if (request.getParameter("key") != null) {
			key = request.getParameter("key");
		}

		ContentsDAO conDAO = ContentsDAO.getInstance();
		if (request.getParameter("cate").equals("all")) {
			all = conDAO.alltheList();
			title = "��ü ��ȭ ���";
		} else if (request.getParameter("cate").equals("new")) {
			all = conDAO.newContents();
			title = "�ű� ������";
		} else {
			all = conDAO.bestContents();
			title = "����Ʈ ������";
		}

		request.setAttribute("title", title);
		request.setAttribute("all", all);
		request.getRequestDispatcher(url).forward(request, response);
	}
}
