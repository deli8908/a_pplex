package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.QnaDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.QnaVO;

public class AdminQnaDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = "admin/admin_qnadetail.jsp";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			int num = Integer.parseInt(request.getParameter("num"));
			QnaVO qnalist = QnaDAO.getInstance().detailList(num);
			
			request.setAttribute("qnalist", qnalist);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
