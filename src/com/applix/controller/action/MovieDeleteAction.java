package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.ContentsDAO;
import com.applix.dao.OrderDAO;
import com.applix.vo.CartVO;
import com.applix.vo.MemberVO;

public class MovieDeleteAction implements Action {

	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String url = "ApplixServlet?command=admin_main";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			ContentsDAO contents= ContentsDAO.getInstance();
			String nums[] = request.getParameterValues("dele");
			
			for (String number : nums) {
				int no = Integer.parseInt(number);
				contents.deleteContents(no);
			}
		}
		response.sendRedirect(url);
	}
}