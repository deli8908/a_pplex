package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.FreeBoardVO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReviewVO;

public class AdminSearchBoardAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			String select = request.getParameter("select");
			String key = request.getParameter("key");
			BoardDAO board = BoardDAO.getInstance();

			if (key != null) {
				if(Integer.parseInt(request.getParameter("boardnum"))==1) {
					ArrayList<ReviewVO> reviewList = board.searchReview(select, key);
					url = "admin/admin_review.jsp";
					request.setAttribute("reviewList", reviewList);
				}else {
					ArrayList<FreeBoardVO> boardList = board.searchBoard(select, key);
					url = "admin/admin_board.jsp";
					request.setAttribute("boardlist", boardList);
				}
			}
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
