package com.applix.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;

public class IndexAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String url = "/index.jsp";

		ContentsDAO conDAO = ContentsDAO.getInstance();
		ArrayList<ContentsVO> newList = conDAO.newContents();
		ArrayList<ContentsVO> bestList = conDAO.bestContents();
		ArrayList<ContentsVO> recoList = conDAO.recoContents();

		request.setAttribute("newList", newList);
		request.setAttribute("bestList", bestList);
		request.setAttribute("recoList", recoList);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
