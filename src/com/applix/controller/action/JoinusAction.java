package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.applix.dao.MemberDAO;
import com.applix.vo.MemberVO;

public class JoinusAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "ApplixServlet?command=login_form";

		MemberDAO memberDAO = MemberDAO.getInstance();
		MemberVO memberVO = new MemberVO();
		memberVO.setCname(request.getParameter("name"));
		memberVO.setId(request.getParameter("id"));
		if (request.getParameter("pw").equals(request.getParameter("pwcheck"))) {
			memberVO.setPw(request.getParameter("pw"));
		}
		memberVO.setBirth(
				request.getParameter("year") + "-" + request.getParameter("month") + "-" + request.getParameter("day"));
		memberVO.setEmail(request.getParameter("email"));
		memberVO.setPhone(request.getParameter("phone"));
		memberDAO.insertMember(memberVO);

		response.sendRedirect(url);
	}
}
