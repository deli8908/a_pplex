package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.ContentsDAO;
import com.applix.vo.ContentsVO;
import com.applix.vo.MemberVO;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

public class AdminUpdateAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = "ApplixServlet?command=admin_main";
		HttpSession session = request.getSession();
		MemberVO adminLogin = (MemberVO) session.getAttribute("adminLogin");
		if (adminLogin == null) {
			url = "ApplixServlet?command=admin_login_form";
		} else {
			String title = request.getParameter("title");
			int sizeLimit = 5 * 1024 * 1024;
			int price2 = 0;
			String savePath = "/images";
			ServletContext context = session.getServletContext();
			String uploadFilePath = context.getRealPath(savePath);
			MultipartRequest multi = new MultipartRequest(request, uploadFilePath, sizeLimit, "UTF-8",
					new DefaultFileRenamePolicy());
			if (multi.getParameter("price2") == null) {
				price2 = 0;
			} else {
				price2 = Integer.parseInt(multi.getParameter("price2").trim());
			}
			ContentsDAO conDAO = ContentsDAO.getInstance();
			ContentsVO conVO = new ContentsVO();
			conVO.setPrice2(price2);
			conVO.setiFrame(multi.getParameter("iFrame"));
			conVO.setImg(multi.getFilesystemName("image"));
			conDAO.contentsUpdate(conVO, title);
		}
		response.sendRedirect(url);
	}
}
