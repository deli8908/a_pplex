package com.applix.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.applix.dao.BoardDAO;
import com.applix.vo.MemberVO;
import com.applix.vo.ReplyVO;

public class ReviewReplyInsertAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String url = null;
		HttpSession session = request.getSession();
		MemberVO loginUser = (MemberVO) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ApplixServlet?command=login_form";
		} else {
			BoardDAO boardDAO = BoardDAO.getInstance();
			ReplyVO replyVO = new ReplyVO();
			int rseq = Integer.parseInt(request.getParameter("rseq").trim());
//			System.out.println("review reply : " + rseq);

			replyVO.setSeqnum(rseq);
			replyVO.setBoard(1);
			replyVO.setContent(request.getParameter("content"));
			replyVO.setR2name(request.getParameter("name"));
			replyVO.setId(request.getParameter("id"));

			boardDAO.insertReply(replyVO);

			url = "ApplixServlet?command=review_detail&rseq=" + rseq;
		}
		response.sendRedirect(url);
	}
}
