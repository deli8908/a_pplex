package com.applix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.applix.vo.CartVO;

public class CartDAO {

	private CartDAO() {
	}

	private static CartDAO instance = new CartDAO();

	public static CartDAO getInstance() {
		return instance;
	}

	public String insertCart(CartVO cartVO, String id) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet re = null;
		String msg = null;
		try {
			conn = DBAction.getInstance().getConnection();
			String sql = "select * from movie_cart where num=" + cartVO.getNum() + " and id='" + id + "'";
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			if (re.next()) {
				msg = "이미 장바구니에 존재하는 품목입니다";
			} else {
				String sql1 = "insert into movie_cart (num, id, title, name, price) values(?,?,?,?,?)";
				pstmt = conn.prepareStatement(sql1);
				pstmt.setInt(1, cartVO.getNum());
				pstmt.setString(2, id);
				pstmt.setString(3, cartVO.getTitle());
				pstmt.setString(4, cartVO.getName());
				pstmt.setInt(5, cartVO.getPrice());

				pstmt.executeUpdate();

			}

		} catch (

		Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return msg;
	}

	public ArrayList<CartVO> cartList(String id) {
		ArrayList<CartVO> cartlist = new ArrayList<CartVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_cart where id='" + id + "' and done='n'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				CartVO cart = new CartVO();
				cart.setId(id);
				cart.setIndate(re.getTimestamp("indate"));
				cart.setName(re.getString("name"));
				cart.setNum(re.getInt("num"));
				cart.setPrice(re.getInt("price"));
				cart.setTitle(re.getString("title"));
				cart.setCseq(re.getInt("cseq"));
				cart.setDone(re.getString("done"));
				cartlist.add(cart);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cartlist;
	}

	public ArrayList<CartVO> cartselectedList(String id, int cseq) {
		ArrayList<CartVO> cartlist = new ArrayList<CartVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_cart where id='" + id + "' and done='n' and cseq=" + cseq;
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				CartVO cart = new CartVO();
				cart.setId(id);
				cart.setIndate(re.getTimestamp("indate"));
				cart.setName(re.getString("name"));
				cart.setNum(re.getInt("num"));
				cart.setPrice(re.getInt("price"));
				cart.setTitle(re.getString("title"));
				cart.setCseq(re.getInt("cseq"));
				cart.setDone(re.getString("done"));
				cartlist.add(cart);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cartlist;
	}
	

}
