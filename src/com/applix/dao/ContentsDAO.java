package com.applix.dao;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.applix.vo.ContentsVO;

public class ContentsDAO {

	private ContentsDAO() {
	}

	private static ContentsDAO instance = new ContentsDAO();

	public static ContentsDAO getInstance() {
		return instance;
	}

	public ContentsVO contentsDetail(int num) {
//		ArrayList<ContentsVO> conList = new ArrayList<ContentsVO>();
		ContentsVO conVO = new ContentsVO();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from moviedb where num=" + num;
			re = stmt.executeQuery(sql);

			if (re.next()) {
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setSaleyn(re.getString("saleyn"));
				conVO.setNewyn(re.getString("newyn"));
				conVO.setBestyn(re.getString("bestyn"));
				conVO.setRecoyn(re.getString("reco"));
				conVO.setiFrame(re.getString("iFrame"));
//				conList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conVO;
	}

	public ContentsVO watchContents(String title) {
//		ArrayList<ContentsVO> conList = new ArrayList<ContentsVO>();
		ContentsVO conVO = new ContentsVO();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			System.out.println("타이틀 : " + title);
			String sql = "select * from moviedb where title='" + title + "'";
			re = stmt.executeQuery(sql);

			if (re.next()) {
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setSaleyn(re.getString("saleyn"));
				conVO.setNewyn(re.getString("newyn"));
				conVO.setBestyn(re.getString("bestyn"));
				conVO.setRecoyn(re.getString("reco"));
				conVO.setiFrame(re.getString("iFrame"));
//				conList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conVO;
	}

	public int findnum(String title) {
//		ContentsVO conVO = new ContentsVO();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		int num = 0;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select num from moviedb where title='" + title + "'";
			re = stmt.executeQuery(sql);

			if (re.next()) {
				num = re.getInt("num");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return num;
	}

	public ArrayList<ContentsVO> newContents() {
		ArrayList<ContentsVO> conList = new ArrayList<ContentsVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from moviedb where newyn='y'";
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ContentsVO conVO = new ContentsVO();
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setNewyn(re.getString("newyn"));
				conList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conList;
	}

	public ArrayList<ContentsVO> bestContents() {
		ArrayList<ContentsVO> bestList = new ArrayList<ContentsVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from moviedb where bestyn='y'";
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ContentsVO conVO = new ContentsVO();
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setSaleyn(re.getString("saleyn"));
				conVO.setBestyn(re.getString("bestyn"));
				bestList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return bestList;
	}

	public ArrayList<ContentsVO> recoContents() {
		ArrayList<ContentsVO> recoList = new ArrayList<ContentsVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from moviedb where reco='y'";
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ContentsVO conVO = new ContentsVO();
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setSaleyn(re.getString("saleyn"));
				recoList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return recoList;
	}

	public ArrayList<ContentsVO> resembleContents(String genre, int num) {
		ArrayList<ContentsVO> resembleList = new ArrayList<ContentsVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from moviedb where genre='" + genre + "' and num!=" + num;
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ContentsVO conVO = new ContentsVO();
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				resembleList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resembleList;
	}

	public ArrayList<ContentsVO> alltheList() {
		ArrayList<ContentsVO> conList = new ArrayList<ContentsVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from moviedb";
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ContentsVO conVO = new ContentsVO();
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setSaleyn(re.getString("saleyn"));
				conVO.setNewyn(re.getString("newyn"));
				conVO.setBestyn(re.getString("bestyn"));
				conVO.setRecoyn(re.getString("reco"));
				conVO.setiFrame(re.getString("iFrame"));
				conList.add(conVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conList;
	}

	public ArrayList<ContentsVO> searchList(String select, String key) {
		ArrayList<ContentsVO> conList = new ArrayList<ContentsVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		String sql = null;
		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			if (select.equals("char")) {
				sql = "select * from moviedb where characters1 like '%" + key + "%' or characters2 like '%" + key
						+ "%' or characters3 like '%" + key + "%'";
				re = stmt.executeQuery(sql);
			} else {
				sql = "select * from moviedb where " + select + " like '%" + key + "%'";
				re = stmt.executeQuery(sql);
			}

			while (re.next()) {
				ContentsVO conVO = new ContentsVO();
				conVO.setMovieNum(re.getInt("num"));
				conVO.setTitle(re.getString("title"));
				conVO.setRunningTime(re.getInt("runningtime"));
				conVO.setAge(re.getString("age"));
				conVO.setDirector(re.getString("director"));
				conVO.setChar1(re.getString("characters1"));
				conVO.setChar2(re.getString("characters2"));
				conVO.setChar3(re.getString("characters3"));
				conVO.setGenre(re.getString("genre"));
				conVO.setSynop(re.getString("synopsis"));
				conVO.setReleaseDate(re.getString("releasedate"));
				conVO.setImg(re.getString("img"));
				conVO.setActive(re.getString("active"));
				conVO.setPrice1(re.getInt("price1"));
				conVO.setPrice2(re.getInt("price2"));
				conVO.setSaleyn(re.getString("saleyn"));
				conVO.setNewyn(re.getString("newyn"));
				conVO.setBestyn(re.getString("bestyn"));
				conVO.setRecoyn(re.getString("reco"));
				conVO.setiFrame(re.getString("iFrame"));
				conList.add(conVO);
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conList;
	}

	public void insertContents(ContentsVO con) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String sql = "insert into moviedb(title, runningtime, age, director, characters1, characters2, characters3, genre, releasedate, price1, price2, iFrame, img, synopsis) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, con.getTitle());
			pstmt.setInt(2, con.getRunningTime());
			pstmt.setString(3, con.getAge());
			pstmt.setString(4, con.getDirector());
			pstmt.setString(5, con.getChar1());
			pstmt.setString(6, con.getChar2());
			pstmt.setString(7, con.getChar3());
			pstmt.setString(8, con.getGenre());
			pstmt.setString(9, con.getReleaseDate());
			pstmt.setInt(10, con.getPrice1());
			pstmt.setInt(11, con.getPrice2());
			pstmt.setString(12, con.getiFrame());
			pstmt.setString(13, con.getImg());
			pstmt.setString(14, con.getSynop());

			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void contentsUpdate(ContentsVO conVO, String title) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet re = null;
		String img = null;

		try {
//			String sql = "update moviedb set img='" + conVO.getImg() + "', iFrame='" + conVO.getiFrame() + "', price2="
//					+ conVO.getPrice2() + " where title='" + title + "'";
			conn = DBAction.getInstance().getConnection();
//			System.out.println(conVO.getImg());
//			System.out.println(title);
			if (conVO.getImg() == null) {
				String sql2 = "select img from moviedb where title='" + title + "'";
				stmt = conn.createStatement();
				re = stmt.executeQuery(sql2);
				if (re.next())
					img = re.getString("img"); // 이미지 수정을 안할 때는 DB에 있는 이미지 경로를 그대로 사용
			} else {
				img = conVO.getImg(); // 이미지 수정할 경우 수정하는 이미지 경로를 가져온다.
			}

			String sql = "update moviedb set img=?,iFrame=?,price2=?,saleyn=? where title=?";
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, img);
			pstmt.setString(2, conVO.getiFrame());
			pstmt.setInt(3, conVO.getPrice2());
			if (conVO.getPrice2() != 0) {
				pstmt.setString(4, "y");
			} else {
				pstmt.setString(4, "n");
			}
			pstmt.setString(5, title);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteContents(int no) {
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "delete from moviedb where num=" + no;
			stmt.executeUpdate(sql);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
