package com.applix.dao;


import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBAction {
	private static DBAction instance = new DBAction();
	Connection conn = null;
	
	private DataSource ds;
	private Context ctx;
	
	public DBAction () {
		
		// 커넥션 풀 사용했을 경우
		try {
			InitialContext initctx = new InitialContext(); // 초기화의 개념
			ctx = (Context)initctx.lookup("java:/comp/env");
			// 환경에 대한 주소값(메모리에 해당 이름으로 바인딩 된 객체를 찾아서 사용하겠다.
			ds = (DataSource)ctx.lookup("jdbc/oracle");
			// (context.xml에서 설정한 Resource name의 값)
			
		}catch(NamingException e) {
			e.printStackTrace();
		}}
		
	public Connection getConnection() {
		try {
			conn= ds.getConnection();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	/*
		// 코드를 직접 입력했을 경우
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://192.168.0.78:3306/user6";
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, "user6", "user6");
			
		// Properties를 이용할 경우
		try {
			Properties properties = new Properties();
			String path = DBAction.class.getResource("database.properties").getPath();
			path = URLDecoder.decode(path, "utf-8");
			properties.load(new FileReader(path));
			String driver = properties.getProperty("driver");
			String url = properties.getProperty("url");
			String username = properties.getProperty("username");
			String password = properties.getProperty("password");
			Class.forName(driver);
			conn = DriverManager.getConnection(url, username, password);
		} catch(Exception e) {
			e.printStackTrace();
		}
		 */
		
	
	public static DBAction getInstance() {
		if (instance == null)
			instance = new DBAction ();
		return instance;
	}
	
	
	public void close() {
		try {
		if (conn != null)
			conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
