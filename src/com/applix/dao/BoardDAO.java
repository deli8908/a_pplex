package com.applix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.applix.vo.FreeBoardVO;
import com.applix.vo.ReplyVO;
import com.applix.vo.ReviewVO;

public class BoardDAO {

	private static BoardDAO instance = new BoardDAO();

	public static BoardDAO getInstance() {
		return instance;
	}

	public ArrayList<ReviewVO> getAllReviews() {
		ArrayList<ReviewVO> reviewlist = new ArrayList<ReviewVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from review";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ReviewVO reVO = new ReviewVO();
				reVO.setIndate(re.getTimestamp("indate"));
				reVO.setRname(re.getString("rname"));
				reVO.setRseq(re.getInt("rseq"));
				reVO.setSubject(re.getString("subject"));
				reVO.setContent(re.getString("content"));
				reVO.setId(re.getString("id"));
				reVO.setTitle(re.getString("title"));
				reviewlist.add(reVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return reviewlist;
	}

	public ArrayList<FreeBoardVO> getAllBoard() {
		ArrayList<FreeBoardVO> boardlist = new ArrayList<FreeBoardVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from freeboard";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				FreeBoardVO boardVO = new FreeBoardVO();
				boardVO.setIndate(re.getTimestamp("indate"));
				boardVO.setFname(re.getString("fname"));
				boardVO.setFseq(re.getInt("fseq"));
				boardVO.setSubject(re.getString("subject"));
				boardVO.setContent(re.getString("content"));
				boardVO.setId(re.getString("id"));
				boardlist.add(boardVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return boardlist;
	}

	public ReviewVO reviewDetail(int rseq) {
		ReviewVO reVO = new ReviewVO();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		try {
			String sql = "select * from review where rseq=" + rseq;
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			if (re.next()) {
				reVO.setIndate(re.getTimestamp("indate"));
				reVO.setRname(re.getString("rname"));
				reVO.setId(re.getString("id"));
				reVO.setRseq(re.getInt("rseq"));
				reVO.setSubject(re.getString("subject"));
				reVO.setContent(re.getString("content"));
				reVO.setTitle(re.getString("title"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return reVO;
	}

	public FreeBoardVO boardDetail(int fseq) {
		FreeBoardVO freeVO = new FreeBoardVO();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		try {
			String sql = "select * from freeboard where fseq=" + fseq;
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			if (re.next()) {
				freeVO.setIndate(re.getTimestamp("indate"));
				freeVO.setFname(re.getString("fname"));
				freeVO.setFseq(re.getInt("fseq"));
				freeVO.setSubject(re.getString("subject"));
				freeVO.setContent(re.getString("content"));
				freeVO.setId(re.getString("id"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return freeVO;
	}

	public ArrayList<ReplyVO> boardDat(int fseq, int boardNum) {
		ArrayList<ReplyVO> replyList = new ArrayList<ReplyVO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		try {
			String sql = "select * from reply where seqnum=" + fseq + " and board=" + boardNum;
			// 해당 게시판에서 해당 글번호에 저장된 댓글을 가져와라
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ReplyVO replyVO = new ReplyVO();
				replyVO.setIndate(re.getTimestamp("indate")); // 작성날짜
				replyVO.setR2name(re.getString("r2name")); // 작성자 이름
				replyVO.setContent(re.getString("content")); // 작성 내용
				replyVO.setSeqnum(re.getInt("seqnum")); // 글 번호
				replyVO.setSeq(re.getInt("seq")); // 댓글 번호(auto)
				replyVO.setId(re.getString("id")); // 아이디
				replyVO.setBoard(boardNum); // 자게 혹은 리뷰 게시판
				replyList.add(replyVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return replyList;
	}

	public void insertReply(ReplyVO rpVO) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String sql = "insert into reply (board, r2name, content, seqnum, id) values(?,?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, rpVO.getBoard());
			pstmt.setString(2, rpVO.getR2name());
			pstmt.setString(3, rpVO.getContent());
			pstmt.setInt(4, rpVO.getSeqnum());
			pstmt.setString(5, rpVO.getId());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void writeReview(ReviewVO reVO) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String sql = "insert into review (subject, rname, content,id, title) values(?,?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, reVO.getSubject());
			pstmt.setString(2, reVO.getRname());
			pstmt.setString(3, reVO.getContent());
			pstmt.setString(4, reVO.getId());
			pstmt.setString(5, reVO.getTitle());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void writeBoard(FreeBoardVO freeVO) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String sql = "insert into freeboard (subject, fname, content,id) values(?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, freeVO.getSubject());
			pstmt.setString(2, freeVO.getFname());
			pstmt.setString(3, freeVO.getContent());
			pstmt.setString(4, freeVO.getId());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<ReviewVO> myreview(String id) {
		ArrayList<ReviewVO> reviewList = new ArrayList<ReviewVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from review where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ReviewVO review = new ReviewVO();
				review.setContent(re.getString("content"));
				review.setIndate(re.getTimestamp("indate"));
				review.setRname(re.getString("rname"));
				review.setRseq(re.getInt("rseq"));
				review.setSubject(re.getString("subject"));
				reviewList.add(review);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return reviewList;
	}

	public ArrayList<FreeBoardVO> myboard(String id) {
		ArrayList<FreeBoardVO> boardList = new ArrayList<FreeBoardVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from freeboard where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				FreeBoardVO board = new FreeBoardVO();
				board.setContent(re.getString("content"));
				board.setIndate(re.getTimestamp("indate"));
				board.setFname(re.getString("fname"));
				board.setFseq(re.getInt("fseq"));
				board.setSubject(re.getString("subject"));
				boardList.add(board);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return boardList;
	}

	public ArrayList<ReviewVO> selectedReview(String title) {
		ArrayList<ReviewVO> reviewList = new ArrayList<ReviewVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from review where title='" + title + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ReviewVO review = new ReviewVO();
				review.setContent(re.getString("content"));
				review.setIndate(re.getTimestamp("indate"));
				review.setRname(re.getString("rname"));
				review.setRseq(re.getInt("rseq"));
				review.setSubject(re.getString("subject"));
				review.setTitle(re.getString("title"));
				reviewList.add(review);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return reviewList;
	}

	public ArrayList<ReviewVO> searchReview(String select, String value) {
		ArrayList<ReviewVO> reviewList = new ArrayList<ReviewVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from review where " + select + " like '%" + value + "%'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				ReviewVO review = new ReviewVO();
				review.setContent(re.getString("content"));
				review.setIndate(re.getTimestamp("indate"));
				review.setRname(re.getString("rname"));
				review.setRseq(re.getInt("rseq"));
				review.setSubject(re.getString("subject"));
				review.setTitle(re.getString("title"));
				review.setId(re.getString("id"));
				reviewList.add(review);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return reviewList;
	}

	public ArrayList<FreeBoardVO> searchBoard(String select, String value) {
		ArrayList<FreeBoardVO> boardList = new ArrayList<FreeBoardVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from freeboard where " + select + " like '%" + value + "%'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				FreeBoardVO board = new FreeBoardVO();
				board.setContent(re.getString("content"));
				board.setIndate(re.getTimestamp("indate"));
				board.setFname(re.getString("fname"));
				board.setFseq(re.getInt("fseq"));
				board.setSubject(re.getString("subject"));
				board.setId(re.getString("id"));
				boardList.add(board);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return boardList;
	}

	public void deletePost(int seq, int boardNum) {
		String sql = null;
		if (boardNum == 1) {
			sql = "delete from review where rseq=" + seq;
		} else if (boardNum == 2) {
			sql = "delete from freeboard where fseq=" + seq;
		}
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteReply(int seq) {
		String sql = "delete from reply where seq=" + seq;
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
