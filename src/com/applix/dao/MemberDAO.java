package com.applix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.applix.vo.MemberVO;

public class MemberDAO {

	private MemberDAO() {
	}

	private static MemberDAO instance = new MemberDAO();

	public static MemberDAO getInstance() {
		return instance;
	}

	public MemberVO loginMember(String userId, String userPw) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		MemberVO memberVO = new MemberVO();
		try {
//			System.out.println("userId : " + userId);
//			System.out.println("userPw : " + userPw);
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from movie_member where id='" + userId + "' and useyn='y'";
			re = stmt.executeQuery(sql);

			if (re.next()) {
				String pw = re.getString("pw");
				if (pw.equals(userPw)) {
					memberVO.setCname(re.getString("cname"));
					memberVO.setId(userId);
					memberVO.setPw(re.getString("pw"));
				} else {
				}
			} else {
				memberVO.setCname(null);
				memberVO.setId(null);
				memberVO.setPw(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return memberVO;
	}

	public MemberVO adminLogin(String id, String pw) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		MemberVO memberVO = new MemberVO();
		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			String sql = "select * from movie_member where id='" + id + "'";

			if (id.equals("admin") & pw.equals("admin")) {
				re = stmt.executeQuery(sql);
				if (re.next()) {
					String dbpw = re.getString("pw");
					if (pw.equals(dbpw)) {
						memberVO.setCname(re.getString("cname"));
						memberVO.setId(id);
						memberVO.setPw(re.getString("pw"));
					}
				}
			} else {
				memberVO = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return memberVO;
	}

	public void insertMember(MemberVO memberVO) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String sql = "insert into movie_member(cname, id, pw, birth, email, phone) values(?,?,?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memberVO.getCname());
			pstmt.setString(2, memberVO.getId());
			pstmt.setString(3, memberVO.getPw());
			pstmt.setString(4, memberVO.getBirth());
			pstmt.setString(5, memberVO.getEmail());
			pstmt.setString(6, memberVO.getPhone());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean checkPw(String current_pw, String id) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet re = null;
		boolean ty = false;
		try {
			String sql = "select pw from movie_member where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				String cur_pw = re.getString("pw");
				if (cur_pw.equals(current_pw))
					ty = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ty;
	}

	public void changePw(String current_pw, String id) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet re = null;
		try {
//			String sql = "select pw from movie_member where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
//			re = stmt.executeQuery(sql);

//			if (re.next()) {
//				String cur_pw = re.getString("pw");
//				if (cur_pw.equals(current_pw)) {
			String sql = "update movie_member set pw=? where id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, current_pw);
			pstmt.setString(2, id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void disabledMember(String userId) {
		Connection conn = null;
		Statement stmt = null;
		String sql = "update movie_member set useyn='n' where id='" + userId + "'";
		try {
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<MemberVO> memberlist() {
		ArrayList<MemberVO> memberlist = new ArrayList<MemberVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_member where id !='admin'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				MemberVO mem = new MemberVO();
				mem.setBirth(re.getString("birth"));
				mem.setCname(re.getString("cname"));
				mem.setEmail(re.getString("email"));
				mem.setId(re.getString("id"));
				mem.setPhone(re.getString("phone"));
				mem.setUseyn(re.getString("useyn"));
				mem.setIndate(re.getTimestamp("indate"));
				memberlist.add(mem);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return memberlist;
	}

	public ArrayList<MemberVO> searchMember(String key) {
		ArrayList<MemberVO> memberlist = new ArrayList<MemberVO>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			conn = DBAction.getInstance().getConnection();
			String sql = "select * from movie_member where cname like '%" + key + "%'";
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				MemberVO mem = new MemberVO();
				mem.setBirth(re.getString("birth"));
				mem.setCname(re.getString("cname"));
				mem.setEmail(re.getString("email"));
				mem.setId(re.getString("id"));
				mem.setPhone(re.getString("phone"));
				mem.setUseyn(re.getString("useyn"));
				mem.setIndate(re.getTimestamp("indate"));
				memberlist.add(mem);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return memberlist;
	}

	public MemberVO mypageMember(String id) {
		MemberVO mem = new MemberVO();

		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_member where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			if (re.next()) {
				mem.setBirth(re.getString("birth"));
				mem.setCname(re.getString("cname"));
				mem.setEmail(re.getString("email"));
				mem.setId(re.getString("id"));
				mem.setPhone(re.getString("phone"));
				mem.setUseyn(re.getString("useyn"));
				mem.setIndate(re.getTimestamp("indate"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (re != null)
					re.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mem;
	}

	public void update_inform(MemberVO memVO) {

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			String sql = "update movie_member set email=?, phone=? where id=?";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, memVO.getEmail());
			stmt.setString(2, memVO.getPhone());
			stmt.setString(3, memVO.getId());

			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
