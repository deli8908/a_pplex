package com.applix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.applix.vo.QnaVO;

public class QnaDAO {

	private QnaDAO() {
	}

	private static QnaDAO instance = new QnaDAO();

	public static QnaDAO getInstance() {
		return instance;
	}

	public ArrayList<QnaVO> allqna(String id) {
		ArrayList<QnaVO> qnalist = new ArrayList<QnaVO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_qna where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				QnaVO qnaVO = new QnaVO();
				qnaVO.setContent(re.getString("content"));
				qnaVO.setQseq(re.getInt("qseq"));
				qnaVO.setReply(re.getString("reply"));
				qnaVO.setReplyyn(re.getString("replyyn"));
				qnaVO.setSubject(re.getString("subject"));
				qnaVO.setQname(re.getString("qname"));
				qnaVO.setIndate(re.getTimestamp("indate"));
				qnaVO.setId(id);
				qnalist.add(qnaVO);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return qnalist;
	}

	public QnaVO detailList(int qseq) {
		QnaVO qnaVO = new QnaVO();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_qna where qseq=" + qseq;
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			if (re.next()) {
				qnaVO.setContent(re.getString("content"));
				qnaVO.setQseq(re.getInt("qseq"));
				qnaVO.setReply(re.getString("reply"));
				qnaVO.setReplyyn(re.getString("replyyn"));
				qnaVO.setSubject(re.getString("subject"));
				qnaVO.setQname(re.getString("qname"));
				qnaVO.setIndate(re.getTimestamp("indate"));
				qnaVO.setIndate2(re.getString("indate2"));
				qnaVO.setId(re.getString("id"));

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return qnaVO;
	}

	public void writeQna(QnaVO qVO) {

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String sql = "insert into movie_qna (subject, content, qname, id) values(?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, qVO.getSubject());
			pstmt.setString(2, qVO.getContent());
			pstmt.setString(3, qVO.getQname());
			pstmt.setString(4, qVO.getId());

			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<QnaVO> adminallqna() {
		ArrayList<QnaVO> qnalist = new ArrayList<QnaVO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_qna";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			re = stmt.executeQuery(sql);

			while (re.next()) {
				QnaVO qnaVO = new QnaVO();
				qnaVO.setContent(re.getString("content"));
				qnaVO.setId(re.getString("id"));
				qnaVO.setQseq(re.getInt("qseq"));
				qnaVO.setReply(re.getString("reply"));
				qnaVO.setReplyyn(re.getString("replyyn"));
				qnaVO.setSubject(re.getString("subject"));
				qnaVO.setQname(re.getString("qname"));
				qnaVO.setIndate(re.getTimestamp("indate"));
				qnalist.add(qnaVO);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return qnalist;
	}

	public void updateReply(QnaVO qna) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String text = time.format(formatter);


		try {
			String sql = "update movie_qna set reply=?, replyyn=?, indate2=? where qseq=?";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, qna.getContent());
			pstmt.setString(2, "y");
			pstmt.setString(3, text);
//			System.out.println("qna insert : " + text);
			pstmt.setInt(4, qna.getQseq());

			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
