<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="UTF-8">
<nav id="info">
	<h5 id="infoTitle">다운로드 기간</h5>
	<h6>구매 - 이용시작 후 7일<br /> 구매 후 7일 이내<br /> 이용 시작하셔야 합니다.<br /> 스트리밍은 다운로드
	기간동안만<br /> 제공되며, MY>내서재에서 이용<br /> 하실 수 있습니다.<br /> 재생가능 기간<br />
	구매 - 다운로드 후 제한없음</h6>
	<p />

	<h5 id="infoTitle">이용정보</h5>
	<h6>DRM-FREE<br /> - 저작권보호장치 미적용<br /> 다운로드 후 파일이동이 가능하며<br /> 재생에 플레이어
	제한이 없습니다.</h6>
	<p />

	<h5 id="infoTitle">자막 - 노출설정 불가능</h5>
	<p />

	<h5 id="infoTitle">PC전용</h5>
	<h6>PC에서만 다운로드 가능합니다.</h6>
	<p />

	<h5>해외에서 스트리밍, 다운로드<br/>
	 불가합니다.</h5>

	<h5 id="infoTitle">지원기기</h5>
	<h6>PC 2대</h6>
	<p />

	<h5 id="infoTitle">파일정보</h5>
	<h6>FHD - 1920x1080, 4.09GB<br /> HD - 1280x720, 2.45GB <br />
	고화질 - 854x480, 1.68GB <br /> 일반화질 - 640x360, 893MB</h6>
	<p />

	<h5 id="infoTitle">환불안내</h5>
	<h6>사용하지 않은 구매대여 상품<br /> 은 7일 이내 MY>구입내역에서<br /> 구매취소를 눌러 직접
	환불하실<br /> 수 있습니다.</h6>
	<p />

</nav>
