<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<jsp:include page="/header.jsp" />
<link href="../main.css" rel="stylesheet">
<style>
#img {
	border: 2px dotted red;
	border-spacing: 1px;
}
</style>
<script>
 	
	function buy() {
		var confirm_msg = confirm("장바구니로 이동하시겠습니까?");
		
		if ('${detailList.saleyn}'=='y') {
			var price = ${detailList.price2};
		} else {var price = ${detailList.price1};} 
		if(confirm_msg == true){
		document.formm.action = "ApplixServlet?command=cart_insert&title="+"${detailList.title}"+"&price="
				+ price + "&num="+"${detailList.movieNum}&gocart=1";
		document.formm.submit();
	}else{
		document.formm.action = "ApplixServlet?command=cart_insert&title="+"${detailList.title}"+"&price="
				+ price + "&num="+"${detailList.movieNum}&gocart=2";
		document.formm.submit();
	}
	}
	
</script>
<section>
	<form name="formm" method="post">
		<h1 id="infomv">${title }</h1>
		<ul id="what">
			<li><a
				href="ApplixServlet?command=selected_review&title=${detailList.title }"
				id="a">리뷰보기</a></li>
		</ul>
		<br />
		<div id="poster">
			<img src="images/${detailList.img }" id="img">
			<jsp:include page="info.jsp" />
		</div>
		<table id="data">
			<tr>
				<td><label id="mvinfo">개봉일</label>${detailList.releaseDate } |
					<label id="mvinfo">러닝타임 </label>${detailList.runningTime }분 | <label
					id="mvinfo">관람가 </label>${detailList.age }| <label id="mvinfo">장르
				</label>${detailList.genre } |</td>
			</tr>
			<tr>
				<td><label id="mvinfo"> 감독 </label>${detailList.director } | <label
					id="mvinfo">주연배우 </label>${detailList.char1 }, ${detailList.char2},
					${detailList.char3 } |</td>
			</tr>
		</table>
		<table id="data">
			<tr>
				<td>${detailList.synop }</td>
			</tr>
		</table>
		<fieldset id="priceInfo">
			<legend id="price">가격 정보</legend>
			<table border='1' id="buybutt">
				<tr>
					<th>가격</th>&nbsp;
					<c:choose>
						<c:when test="${detailList.price2==0 }">
							<td><fmt:formatNumber value="${detailList.price1 }"
									type="currency" /></td>
							<td style='width: 20%;'><input type="submit" value="장바구니담기"
								class="but" onclick="buy()"></td>
						</c:when>
						<c:otherwise>
							<td><fmt:formatNumber value="${detailList.price2 }"
									type="currency" /></td>
							<td style='width: 20%;'><input type="submit" value="장바구니담기"
								class="but" onclick="buy()"></td>
						</c:otherwise>
					</c:choose>
			</table>
		</fieldset>
		<fieldset id="iframe">
			<legend>Trailer</legend>
			<iframe width="100%" height="450" src="${detailList.iFrame }"
				frameborder="1"
				allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowfullscreen></iframe>
		</fieldset>
		<div class="content-wrapper">
			<fieldset id="detail_fieldset">
				<legend id="recommend">비슷한 장르의 영화</legend>
				<div class="gridwrapper">
					<table>
						<tr>
							<c:forEach items="${resembleList }" var="resemble">
								<th><a
									href="ApplixServlet?command=contents_detail&num=${resemble.movieNum }">
										<img src="images/${resemble.img }"
										style='width: 150px; height: 200px;'><br>
								</a></th>
							</c:forEach>
						</tr>
					</table>

					<a href="ApplixServlet?command=index"><input type="button"
						value="메인으로" class="button"></a>
				</div>
			</fieldset>
		</div>
	</form>
</section>
<jsp:include page="/footer.jsp" />