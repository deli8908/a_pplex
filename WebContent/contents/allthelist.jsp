<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.review_h1 {
	font-size: 300%;
}

#review_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#review_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#review_title:hover {
	text-decoration: underline;
}

#reviewList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

#searchSection {
	display: inline;
	margin: 10px 10px 0 0;
	float : right;
	margin-bottom : 30px;
}

#button {
	margin-top: 50px;
	height: 30px;
	width: 60px;
	border-collapse: collapse;
	border: 1px solid #666;
	box-shadow: 3px 3px 2px #ccc;
	font-size: small;
	background-repeat: repeat-x;
	background-position: center center;
	color: #fff;
	margin-right: 10px;
	text-align: center;
	background-color: #CC3D3D;
	font-weight: bold;
	display: inline;
}

</style>

<script>
function go_search(){
		document.formm.action = "ApplixServlet?command=search_contents";
		document.formm.submit();
	}

</script>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="review_table">
			<tr>
				<td><h1 class="review_h1">${title}</h1></td>
			</tr>
		</table>

		<span id="searchSection"><select name="select">
				<option value="title">제목</option>
				<option value="genre">장르</option>
				<option value="director">감독</option>
				<option value="char">배우</option>
		</select><input type="text" id="textfield" name="key"> <input
			type="submit" name="btn_search" value="검색" id="button"
			onclick="go_search()" onkeypress="go_search()"> </span>

		<table border=1 id="reviewList">
			<tr>
				<th>영화 제목</th>
				<th>관람가</th>
				<th>감독</th>
				<th>주연배우</th>
				<th>장르</th>
			</tr>
			<c:forEach items="${ all}" var="all">
				<tr>
					<td><a
						href="ApplixServlet?command=contents_detail&num=${all.movieNum}">${all.title}</a></td>
					<td>${all.age}</td>
					<td>${all.director}</td>
					<td>${all.char1},&nbsp;${all.char2},&nbsp;${all.char3}</td>
					<td>${all.genre}</td>
				</tr>
			</c:forEach>
		</table>
	</form>
</nav>
<jsp:include page="/footer.jsp" />