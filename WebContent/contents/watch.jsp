<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<jsp:include page="/header.jsp" />
<link href="../main.css" rel="stylesheet">
<style>
#img {
	border: 2px dotted red;
	border-spacing: 1px;
}

.button {
	float: right;
}

iframe{
margin-top: 20px;
}
</style>
<script>
	
</script>
<section>
	<form name="formm" method="post">
		<h1 id="infomv">${title }</h1>
		<ul id="what">
			<li><a
				href="ApplixServlet?command=selected_review&title=${detailList.title}"
				id="a">리뷰보기</a></li>
		</ul>
		<br />
		<iframe width="100%" height="450" src="${detailList.iFrame }"
				frameborder="1"
				allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowfullscreen></iframe>
		<a href="ApplixServlet?command=index"><input type="button"
			value="메인으로" class="button"></a>
		<a href="ApplixServlet?command=orderlist"><input type="button"
			value="목록으로" class="button"></a>
	</form>
</section>
<jsp:include page="/footer.jsp" />