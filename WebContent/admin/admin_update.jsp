<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<section>
<script>
function update_info() {
	alert("수정되었습니다.");
	var theForm = document.formm;
	theForm.encoding = "multipart/form-data";
		theForm.action = "ApplixServlet?command=admin_update&title="+"${detailList.title }";
		theForm.submit();
	}
</script>
	<center>
		<h1 class="manage_title">상품 수정</h1>
	</center>
	<form name="formm" method="post" enctype="multipart/form-data">
		<p />
		<table id="man_update" width=100%>
			<tr>
				<th width=15%>상품이미지</th>
				<td><img src="images/${detailList.img }"></td>
				<td colspan="2"><input type="file" name="image"></td>
			</tr>
			<tr>
				<th>타이틀</th>
				<td>${detailList.title }</td>
				<th>러닝타임</th>
				<td>${detailList.runningTime }</td>
			</tr>
			<tr>
				<th>관람가</th>
				<td>${detailList.age}</td>
				<th>장르</th>
				<td>${detailList.genre }</td>
			</tr>
			<tr>
				<th>감독</th>
				<td>${detailList.director}</td>
			</tr>
			<tr>
				<th>배우1</th>
				<td>${detailList.char1}</td>
			</tr>
			<tr>
				<th>배우2</th>
				<td>${detailList.char2 }</td>
			</tr>
			<tr>
				<th>배우3</th>
				<td>${detailList.char3 }</td>
			</tr>
			<tr>
				<th>개봉일</th>
				<td>${detailList.releaseDate }</td>
			</tr>
			<tr>
				<th>원가[A]</th>
				<td>${detailList.price1 }</td>
				<th>할인가[B]</th>
				<td><input type="text" name="price2" value="${detailList.price2 }"></td>
			</tr>
			<tr>
				<th>예고편 주소</th>
				<td><input type="text" name="iFrame" value="${detailList.iFrame }"></td>
			</tr>
			<tr>
				<th>시놉시스</th>
				<td>${detailList.synop }</td>
			</tr>
		</table>
		<div id="butt">
			<input type="submit" value="수정" class="button2" onclick="update_info()">
			<a href="ApplixServlet?command=admin_main"><input
				type="button" value="취소" class="button2"></a>
		</div>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />
