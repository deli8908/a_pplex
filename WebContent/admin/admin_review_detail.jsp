<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<style>
.button {
	height: 30px;
	width: 120px;
	border-collapse: collapse;
	border: 1px solid #666;
	font-size: small;
	background-repeat: repeat-x;
	background-position: center center;
	color: #fff;
	text-align: center;
	background-color: red;
	font-weight: bold;
	font-family: "S-Core Dream 4";
	margin-left : 40%;
}
</style>
<script>
	
	function review_delete(){
		self.close();
		document.formm.action = "ApplixServlet?command=admin_review_delete&rseq="+${reviewList.rseq};
		document.formm.submit();
		opener.parent.location.reload();
	}
	
</script>
<section>
	<center>
		<h1 class="manage_title">리뷰게시판</h1>
	</center>
	<form name="formm" method="post">
		<p />
		<table id="memberList">
			<tr>
				<th>글번호</th>
				<td>${reviewList.rseq }</td>
			</tr>
			<tr>
				<th>작성자 아이디</th>
				<td>${reviewList.id }</td>
			</tr>
			<tr>
				<th>이름</th>
				<td>${reviewList.rname }</td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><fmt:formatDate value="${reviewList.indate }" type="date" /></td>
			</tr>
			<tr>
				<th>제목</th>
				<td>${reviewList.subject }</td>
			</tr>
			<tr>
				<th>내용</th>
				<td>${reviewList.content }</td>
			</tr>

			<%-- 	<c:choose>
					<c:when test="${memberListSize<=0}">
						<tr>
							<td width="100%" colspan="7" align="center" height="23">등록된
								회원이 없습니다</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${memberList }" var="memberVO">
							<tr>
								<td><c:choose>
										<c:when test='${memberVO.useyn=="y" }'>
											<span style="font-weight: bold;">${memberVO.id }</span>
						<input type="checkbox" name="result" disabled="disabled">
						</c:when>
										<c:otherwise>
											<span style="font-weight: bold; ">${memberVO.id }</span>
						<input type="checkbox" checked="checked" disabled="disabled">
						</c:otherwise>
									</c:choose></td> --%>
		</table>
		<input type="submit" value="글삭제" onclick="review_delete()"
			class="button">
	</form>
</section>
