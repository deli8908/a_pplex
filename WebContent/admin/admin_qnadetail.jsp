<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<style>
.button {
	height: 30px;
	width: 120px;
	border-collapse: collapse;
	border: 1px solid #666;
	font-size: small;
	background-repeat: repeat-x;
	background-position: center center;
	color: #fff;
	text-align: center;
	background-color: red;
	font-weight: bold;
	font-family: "S-Core Dream 4";
	margin-left: 40%;
}

#memberList2 {
	border-collapse: collapse;
	border-top: 2px solid black;
	border-bottom: 1px solid black;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	text-align: center;
}

#memberList2 tr {
	border-collapse: collapse;
	margin: 40px;
	height: 50px;
	padding: 30px;
	font-size: small;
	text-align: center;
	font-size: small;
}

.button2 {
	float: right;
	background-color: red;
}
</style>
<script>

	function insert_qna_reply() {
		document.formm2.action = "ApplixServlet?command=insert_qna_reply&qseq=${qnalist.qseq}";
		document.formm2.submit();
	}
</script>
<section>
	<center>
		<h1 class="manage_title">Q&A 게시글</h1>
	</center>
	<form name="formm" method="post">
		<p />
		<table id="memberList">
			<tr>
				<th>글번호</th>
				<td>${qnalist.qseq }</td>
			</tr>
			<tr>
				<th>작성자 아이디</th>
				<td>${qnalist.id }</td>
			</tr>
			<tr>
				<th>이름</th>
				<td>${qnalist.qname }</td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><fmt:formatDate value="${qnalist.indate }" type="date" /></td>
			</tr>
		</table>
	</form>
	<form name="formm2" method="post">
		<table id="memberList2" width=100%>
			<tr>
				<th width=60%>제목</th>
				<td>${qnalist.subject }</td>
			</tr>
			<tr>
				<th>내용</th>
				<td>${qnalist.content }</td>
			</tr>
			<c:choose>
				<c:when test="${qnalist.replyyn  eq 'y' }">
					<tr>
						<th>답변 여부</th>
						<td>완료</td>
					</tr>
					<tr>
						<th>답변 일자</th>
						<td>${qnalist.indate2}</td>
					</tr>
					<tr height="50px">
						<th style='color : red'>답변 내용</th>
						<td style='color : red; font-weight : bold;'>${qnalist.reply }</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<th>답변 여부</th>
						<td>미완료</td>
					</tr>
					<tr>
						<th>답변달기</th>
						<td><textarea rows="20" cols="100" name="txarea"></textarea></td>
					</tr>
					<tr>
						<center>
							<td colspan="2"><input type="submit" value="등록"
								class="button" onclick="insert_qna_reply()"></td>
						</center>
					</tr>
				</c:otherwise>
			</c:choose>
			<%-- 	<c:choose>
					<c:when test="${memberListSize<=0}">
						<tr>
							<td width="100%" colspan="7" align="center" height="23">등록된
								회원이 없습니다</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${memberList }" var="memberVO">
							<tr>
								<td><c:choose>
										<c:when test='${memberVO.useyn=="y" }'>
											<span style="font-weight: bold;">${memberVO.id }</span>
						<input type="checkbox" name="result" disabled="disabled">
						</c:when>
										<c:otherwise>
											<span style="font-weight: bold; ">${memberVO.id }</span>
						<input type="checkbox" checked="checked" disabled="disabled">
						</c:otherwise>
									</c:choose></td> --%>
		</table>
			<a href="ApplixServlet?command=admin_qna"><input
			type="button" value="목록으로" class="button2"></a>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />