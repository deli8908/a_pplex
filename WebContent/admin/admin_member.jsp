<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<section>
<script>
function go_search(){
	document.formm.action = "ApplixServlet?command=search_member";
	document.formm.submit();
}
</script>
	<center>
		<h1 class="manage_title">회원 리스트</h1>
	</center>
	<form name="formm" method="post">
		<p />
		<div id="searchSection">
			회원 이름 <input type="text" id="textfield" name="key"> <input
				type="submit" name="btn_search" value="검색" class="button"
				onclick="go_search()" onkeypress="go_search()">
		</div>
		<table id="memberList">
			<tr>
				<th>아이디</th>
				<th>이름</th>
				<th>이메일</th>
				<th>연락처</th>
				<th>가입일</th>
				<th>계정사용여부</th>
			</tr>

			<c:forEach items="${memberlist }" var="memberlist">
				<tr>
					<td>${memberlist.id }</td>
					<td>${memberlist.cname }</td>
					<td>${memberlist.email }</td>
					<td>${memberlist.phone }</td>
					<td><fmt:formatDate value="${memberlist.indate }" type="date"/></td>
					<td>${memberlist.useyn }</td>
				</tr>
				</c:forEach>
				<%-- 	<c:choose>
					<c:when test="${memberListSize<=0}">
						<tr>
							<td width="100%" colspan="7" align="center" height="23">등록된
								회원이 없습니다</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${memberList }" var="memberVO">
							<tr>
								<td><c:choose>
										<c:when test='${memberVO.useyn=="y" }'>
											<span style="font-weight: bold;">${memberVO.id }</span>
						<input type="checkbox" name="result" disabled="disabled">
						</c:when>
										<c:otherwise>
											<span style="font-weight: bold; ">${memberVO.id }</span>
						<input type="checkbox" checked="checked" disabled="disabled">
						</c:otherwise>
									</c:choose></td> --%>
		</table>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />
