<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<script>
	function okay() {
		var theForm = document.formm;
		theForm.encoding = "multipart/form-data";
		if (theForm.age.value == 'no' | theForm.genre.value == 'no') {
			alert("선택 박스를 다시 선택해주세요.");
		} else {
			theForm.action = "ApplixServlet?command=admin_insert";
			theForm.submit();
		}
	}
</script>
<section>
	<center>
		<h1 class="manage_title">상품 등록</h1>
	</center>
	<form name="formm" method="post" enctype="multipart/form-data">
		<p />
		<table id="contentsInsert">
			<tr>
				<th>타이틀</th>
				<td colspan="2"><input type="text" name="title"
					placeholder="제목을 입력하세요"></td>
			</tr>
			<tr>
				<th>러닝타임</th>
				<td><input type="text" name="running" placeholder="상영시간을 입력하세요"></td>
			</tr>
			<tr>
				<th>관람가</th>
				<td><select name="age">
						<option value="no">===선택===</option>
						<option value="전체 관람가">전체 관람가</option>
						<option value="12세 관람가">12세 관람가</option>
						<option value="15세 관람가">15세 관람가</option>
						<option value="청소년 관람불가">청소년 관람불가</option>
				</select></td>
			</tr>
			<tr>
				<th>감독</th>
				<td colspan="2"><input type="text" name="direct"
					placeholder="감독명을 입력하세요"></td>
			</tr>
			<tr>
				<th>배우1</th>
				<td><input type="text" name="act1" placeholder="주연배우1을 입력하세요"></td>
			</tr>
			<tr>
				<th>배우2</th>
				<td><input type="text" name="act2" placeholder="주연배우2을 입력하세요"></td>
			</tr>
			<tr>
				<th>배우3</th>
				<td><input type="text" name="act3" placeholder="주연배우3을 입력하세요"></td>
			</tr>
			<tr>
				<th>장르</th>
				<td><select name="genre">
						<option value="no">==장르선택==</option>
						<option value="가족">가족</option>
						<option value="드라마">드라마</option>
						<option value="로맨스">로맨스</option>
						<option value="액션">액션</option>
						<option value="코미디">코미디</option>
						<option value="판타지">판타지</option>
						<option value="공포/스릴러">공포/스릴러</option>
				</select></td>
			</tr>
			<tr>
				<th>개봉일</th>
				<td><input type="text" name="indate" placeholder="개봉날짜를 입력하세요"></td>
			</tr>
			<tr>
				<th>원가[A]</th>
				<td><input type="text" name="price1" placeholder="0"></td>
				<th>할인가[B]</th>
				<td><input type="text" name="price2" value="0"></td>
			</tr>
			<tr>
				<th>예고편 주소</th>
				<td colspan="2"><input type="text" name="iFrame"></td>
			</tr>
			<tr>
				<th>상품이미지</th>
				<td colspan="2"><input type="file" name="image"></td>
			</tr>
			<tr>
				<th>시놉시스</th>
				<td><textarea rows="8" cols="80" name="synop"></textarea>
			</tr>
		</table>
		<div id="butt">
			<input type="submit" value="등록" class="button2" onclick="okay()">
			<a href="ApplixServlet?command=admin_main"><input type="button" value="취소" class="button2" name="cancel"></a>
		</div>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />
