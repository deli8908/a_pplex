<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<section>
	<script>
	function go_delete() {
		var count = 0;
		if (document.formm.dele.length == undefined) {
			if (document.formm.dele.checked == true) {
				count++;
			}
		}
		for (var i = 0; i < document.formm.dele.length; i++) {
			if (document.formm.dele[i].checked == true) {
				count++;
			}
		}
		if (count == 0) {
			alert("삭제할 항목을 선택해주세요.");
		} else {
			document.formm.action = "ApplixServlet?command=movie_delete";
			document.formm.submit();
		}
	}
	</script>
	<center>
		<h1 class="manage_title">보유 컨텐츠 리스트</h1>
	</center>
	<form name="formm" method="post">
<!-- 	 enctype="multipart/form-data"> -->
		<p />
		<table id="contentsList">
			<tr>
				<th>영화등록번호</th>
				<th>타이틀</th>
				<th>관람가</th>
				<th>감독</th>
				<th>주연배우</th>
				<th>장르</th>
				<th>개봉일</th>
				<th>판매가</th>
				<th>삭제</th>
			</tr>
			<c:forEach items="${conList }" var="conVO">
				<tr>
					<td>${conVO.movieNum}</td>
					<td><a
						href="ApplixServlet?command=update_form&num=${conVO.movieNum }">${conVO.title }</a></td>
					<td>${conVO.age }</td>
					<td>${conVO.director}</td>
					<td>${conVO.char1},${conVO.char2},${conVO.char3 }</td>
					<td>${conVO.genre }</td>
					<td>${conVO.releaseDate}</td>
					<c:choose>
						<c:when test="${conVO.price2==0 }">
							<td><fmt:formatNumber value="${conVO.price1 }"
									type="currency" /></td>
						</c:when>
						<c:otherwise>
							<td><fmt:formatNumber value="${conVO.price2 }"
									type="currency" /></td>
						</c:otherwise>
					</c:choose>
					<td><input type="checkbox" name="dele" value="${conVO.movieNum}"></td>
				</tr>
			</c:forEach>
		</table>
		<div id="butt">
			<a href="ApplixServlet?command=admin_insert_contents"><input
				type="button" value="등록" class="button2"></a> <input
				type="submit" value="삭제" class="button2" name="cseq" onclick="go_delete()">
		</div>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />
