<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<link href="admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<section>
	<center>
		<h1 class="manage_title">회원 리스트</h1>
	</center>
	<form name="formm" method="post">
		<p />
		<div id="searchSection"> 회원 이름 <input type="text"
			id="textfield" name="key"> <input type="button"
			name="btn_search" value="검색" class="button" onclick="go_search()"
			onkeypress="go_search()">
		</div>
		<table id="memberList">
			<tr>
				<th>아이디(탈퇴여부)</th>
				<th>이름</th>
				<th>이메일</th>
				<th>생년월일</th>
				<th>전화</th>
				<th>가입일</th>
			</tr>
			<%-- 	<c:choose>
					<c:when test="${memberListSize<=0}">
						<tr>
							<td width="100%" colspan="7" align="center" height="23">등록된
								회원이 없습니다</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${memberList }" var="memberVO">
							<tr>
								<td><c:choose>
										<c:when test='${memberVO.useyn=="y" }'>
											<span style="font-weight: bold;">${memberVO.id }</span>
						<input type="checkbox" name="result" disabled="disabled">
						</c:when>
										<c:otherwise>
											<span style="font-weight: bold; ">${memberVO.id }</span>
						<input type="checkbox" checked="checked" disabled="disabled">
						</c:otherwise>
									</c:choose></td> --%>
			<tr>
				<td>아이디<input type="checkbox" name="result" disabled="disabled"></td>
				<td>이름</td>
				<td>이메일</td>
				<td>생년월일</td>
				<td>전화</td>
				<td>가입일</td>
			</tr>
		</table>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />
