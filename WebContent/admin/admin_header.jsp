<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<header>
		<nav class="nav1">
			<a href="ApplixServlet?command=admin_main"><h1 id="maintitle">Applix</h1></a>
		</nav>
		<nav class="nav2">
			<c:choose>
				<c:when test="${adminLogin==null}">
					<ul id="mainbar">
					</ul>
				</c:when>
				<c:otherwise>
					<ul>
						<li id="admin_list">관리 메뉴 | <a
							href="ApplixServlet?command=admin_insert_contents">상품등록</a> | <a
							href="ApplixServlet?command=admin_member">회원목록</a> | <a href="ApplixServlet?command=admin_order">구매
								관리 </a> | <a href="ApplixServlet?command=admin_review">게시글 관리 </a>| <a
							href="ApplixServlet?command=logout">로그아웃</a></li>
					</ul>
				</c:otherwise>
			</c:choose>
		</nav>
		<div class="clear"></div>
	</header>
</html>