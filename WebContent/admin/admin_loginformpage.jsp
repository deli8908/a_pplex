<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<link href="admin/admin_main.css" rel="stylesheet">

<jsp:include page="admin_header.jsp" />
<section>
	<script>
		function login_action() {
			document.formm.action = "ApplixServlet?command=admin_login";
			document.formm.submit();
		}
	</script>
	<form name="formm" method="post">
		<center>
			<h1 class="login_h1">ADMIN LOGIN</h1>
			<p />
			<table id="login_table" border=2px width=30%>
				<tr>
					<th width=30%>아이디</th>
					<td><input type="text" size=35px name="id"
						style='width: 100%; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th width=30%>비밀번호</th>
					<td><input type="password" size=35px name="pw"
						style='width: 100%; height: 50px; padding: 0;'></td>
				</tr>
			</table>
			<input type="submit" value="로그인" name="login" class="button"
				onclick="login_action()"><input type="button" value="취소"
				name="cancel" class="button">
		</center>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />