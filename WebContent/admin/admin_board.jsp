<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<link href="admin/admin_main.css" rel="stylesheet">
<jsp:include page="admin_header.jsp" />
<style>
.button {
	margin-top: 50px;
	height: 30px;
	width: 120px;
	border-collapse: collapse;
	border: 1px solid #666;
	font-size: small;
	background-repeat: repeat-x;
	background-position: center center;
	color: #fff;
	text-align: center;
	background-color: red;
	font-weight: bold;
	display: inline;
	font-family: "S-Core Dream 4";
}


label{
font-weight : bold;
color : blue;
}

</style>
<script>
	function go_search() {
		document.formm.action = "ApplixServlet?command=search_board&boardnum=2";
		document.formm.submit();
	}

	function read_review(aa) {
 				var url = 'ApplixServlet?command=admin_board_detail&fseq='+aa; 
				window.open(url, '', 'width=800, height=500');
	}
</script>
<%@ include file="sub_menu.jsp"%>
<section>
	<center>
		<h1 class="manage_title">자유게시판 리스트</h1>
	</center>
	<form name="formm" method="post">
		<p />
		<div id="searchSection">
			<span> <select name="select">
					<option value="id">회원 아이디</option>
					<option value="fname">회원 이름</option>
					<option value="subject">글제목</option>
					<option value="content">내용</option>
			</select> <input type="text" id="textfield" name="key"> <input
				type="button" name="btn_search" value="검색" class="button"
				onclick="go_search()" onkeypress="go_search()"></span>
		</div>
		<table id="memberList">
			<tr>
				<th>글번호</th>
				<th>작성자 아이디</th>
				<th>이름</th>
				<th>제목</th>
				<th>작성일</th>
				<th>상세보기</th>
			</tr>

			<c:forEach items="${boardlist }" var="boardlist">
				<tr>
					<td><input type="hidden" name="a" value="${boardlist.fseq }">
						${boardlist.fseq }</td>
					<td>${boardlist.id }</td>
					<td>${boardlist.fname }</td>
					<td>${boardlist.subject }</td>
					<td><fmt:formatDate value="${boardlist.indate }" type="date" /></td>
					<td><label onclick="read_review(${boardlist.fseq})">보기</label></td>
				</tr>
			</c:forEach>
			<%-- 	<c:choose>
					<c:when test="${memberListSize<=0}">
						<tr>
							<td width="100%" colspan="7" align="center" height="23">등록된
								회원이 없습니다</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${memberList }" var="memberVO">
							<tr>
								<td><c:choose>
										<c:when test='${memberVO.useyn=="y" }'>
											<span style="font-weight: bold;">${memberVO.id }</span>
						<input type="checkbox" name="result" disabled="disabled">
						</c:when>
										<c:otherwise>
											<span style="font-weight: bold; ">${memberVO.id }</span>
						<input type="checkbox" checked="checked" disabled="disabled">
						</c:otherwise>
									</c:choose></td> --%>
		</table>
	</form>
</section>
<jsp:include page="admin_footer.jsp" />
