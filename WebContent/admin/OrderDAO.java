package com.applix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.applix.vo.CartVO;
import com.applix.vo.OrderVO;

public class OrderDAO {
	private static OrderDAO instance = new OrderDAO();

	public static OrderDAO getInstance() {
		return instance;
	}

	public void insertOrder(ArrayList<CartVO> cartlist, int i) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;

		try {
			String sql = "insert into movie_order (num, id, title, cname, price, cseq) values(?,?,?,?,?,?)";
			conn = DBAction.getInstance().getConnection();
			pstmt = conn.prepareStatement(sql);
			for (CartVO cart : cartlist) {
				pstmt.setInt(1, cart.getNum());
				pstmt.setString(2, cart.getId());
				pstmt.setString(3, cart.getTitle());
				pstmt.setString(4, cart.getName());
				pstmt.setInt(5, cart.getPrice());
				pstmt.setInt(6, cart.getCseq());

				pstmt.executeUpdate();
				pstmt.close();

				if (i == 1) {
					updateOrder(cart.getCseq(), cart.getId());
				} else {
					deleteOrder(cart.getCseq(), cart.getId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void updateOrder(int cseq, String id) {
		Connection conn = null;
		Statement stmt = null;

		try {
			String sql = "update movie_cart set done='y' where id='" + id + "' and cseq=" + cseq;
			conn = DBAction.getInstance().getConnection(); // pstmt.setInt(1, cseq);

			stmt = conn.createStatement();
			stmt.executeUpdate(sql);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public void deleteOrder(int cseq, String id) {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			String sql = "delete from movie_cart where id='" + id + "' and cseq=" + cseq;
			conn = DBAction.getInstance().getConnection(); // pstmt.setInt(1, cseq);
			
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	public ArrayList<OrderVO> orderlist(String id) {
		ArrayList<OrderVO> orderlist = new ArrayList<OrderVO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;

		try {
			String sql = "select * from movie_order where id='" + id + "'";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();

			re = stmt.executeQuery(sql);

			while (re.next()) {
				OrderVO order = new OrderVO();
				order.setCname(re.getString("cname"));
				order.setId(re.getString("id"));
				order.setIndate(re.getTimestamp("indate"));
				order.setNum(re.getInt("num"));
				order.setOseq(re.getInt("oseq"));
				order.setPrice(re.getInt("price"));
				order.setTitle(re.getString("title"));
				orderlist.add(order);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return orderlist;
	}
	public ArrayList<OrderVO> getAllOrder() {
		ArrayList<OrderVO> orderlist = new ArrayList<OrderVO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet re = null;
		
		try {
			String sql = "select * from movie_order";
			conn = DBAction.getInstance().getConnection();
			stmt = conn.createStatement();
			
			re = stmt.executeQuery(sql);
			
			while (re.next()) {
				OrderVO order = new OrderVO();
				order.setCname(re.getString("cname"));
				order.setId(re.getString("id"));
				order.setIndate(re.getTimestamp("indate"));
				order.setNum(re.getInt("num"));
				order.setOseq(re.getInt("oseq"));
				order.setPrice(re.getInt("price"));
				order.setTitle(re.getString("title"));
				orderlist.add(order);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return orderlist;
	}
}
