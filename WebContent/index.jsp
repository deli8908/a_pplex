<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<jsp:include page="header.jsp" />
<style>
::-webkit-scrollbar {
	width: 8px;
	height: 8px;
	border: 3px solid #fff;
}

::-webkit-scrollbar-button:start:decrement, ::-webkit-scrollbar-button:end:increment
	{
	display: block;
	height: 10px;
}

::-webkit-scrollbar-track {
	background: #efefef;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 4px rgba(0, 0, 0, .2);
	background-color: red;
}

::-webkit-scrollbar-thumb {
	background-color: red;
	height: 50px;
	width: 50px;
	background: rgba(0, 0, 0, .2);
	-webkit-border-radius: 8px;
	border-radius: 8px;
	-webkit-box-shadow: inset 0 0 4px rgba(0, 0, 0, .1)
}
</style>
<div class="clear"></div>
<script>
if(${msg}!=null){
window.onload = function(){
	alert(${msg});
}
}
	
</script>
<section>
	<form name="formm" method="get">
		<iframe width="100%" height="500"
			src="https://www.youtube.com/embed/BaSf-ddZxB8?amp; autoplay=1"
			frameborder="0"
			allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
			allowfullscreen></iframe>
		<article class="main">
			<div class="content-wrapper">
				<fieldset id="fieldset">
					<legend id="newCon">NEW CONTENTS </legend>
					<div class="gridwrapper">
						<table>
							<tr>
								<c:forEach items="${newList}" var="newitem">
									<th><a
										href="ApplixServlet?command=contents_detail&num=${newitem.movieNum }">
											<img src="images/${newitem.img }"
											style="width: 150px; height: 200px;"><br>
									</a></th>
								</c:forEach>
							</tr>
						</table>
					</div>
				</fieldset>
			</div>
		</article>
		<article class="main">
			<div class="content-wrapper">
				<fieldset id="fieldset">
					<legend id="bestCon">BEST CONTENTS </legend>
					<div class="gridwrapper">
						<table>
							<tr>
								<c:forEach items="${bestList}" var="bestitem">
									<th><a
										href="ApplixServlet?command=contents_detail&num=${bestitem.movieNum }">
											<img src="images/${bestitem.img }"
											style="width: 150px; height: 200px;"><br>
									</a></th>
								</c:forEach>
							</tr>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="content-wrapper">
				<fieldset id="fieldset">
					<legend id="recommended">RECOMMENDED </legend>
					<div class="gridwrapper">
						<table>
							<tr>
								<c:forEach items="${recoList}" var="recoitem">
									<th><a
										href="ApplixServlet?command=contents_detail&num=${recoitem.movieNum }">
											<img src="images/${recoitem.img }"
											style="width: 150px; height: 200px;"><br>
									</a></th>
								</c:forEach>
							</tr>
						</table>
					</div>
				</fieldset>
			</div>
	</form>
</section>
<jsp:include page="footer.jsp" />



