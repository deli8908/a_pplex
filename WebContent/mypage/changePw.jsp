<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<jsp:include page="/header.jsp" />
<script>
	function change() {
		document.formm.action = "ApplixServlet?command=change_password";
		document.formm.submit();
	}
</script>
<link href="../main.css" rel="stylesheet">
<span><%@ include file="sub_menu.jsp"%>
	<nav id="mainbody">
		<section>
			<form name="formm" method="post">
				<p />
				<fieldset>
					<legend id="malegend">MY PAGE</legend>
					<table id="mypage_table">
						<tr>
							<th>현재 비밀번호</th>
							<td><input type="password" name="current_pw"></td>
						</tr>
						<tr>
							<th>변경할 비밀번호</th>
							<td><input type="password" name="new_pw"></td>
						</tr>
						<tr>
							<th>비밀번호 확인</th>
							<td><input type="password" name="check_pw"></td>
						</tr>

					</table>
					<div id="button_id">
						<input type="submit" value="변경" name="join" class="button"
							onclick="change()"> <a
							href="ApplixServlet?command=mypage"><input type="button"
							value="취소" name="cancel" class="button"></a>	<a href="ApplixServlet?command=remove_account"><input
							type="button" value="회원탈퇴" name="remove" class="button"></a>
					</div>
				</fieldset>
			</form>
		</section>
	</nav> </span>
<jsp:include page="/footer.jsp" />