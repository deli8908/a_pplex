<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.qna_h1 {
	font-size: 300%;
}

#qna_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#qna_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#qna_title:hover {
	text-decoration: underline;
}

#qnaList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

.button {
	float: right;
}
</style>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="qna_table">
			<tr>
				<td><h1 class="qna_h1">Q&A</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="qnaList">
			<tr>
				<th>글번호</th>
				<th>제 목</th>
				<th>작성자</th>
				<th>작성일</th>
				<th>답변여부</th>
			</tr>
			<c:forEach items="${qnalist}" var="qnalist">
				<tr>
					<td>${qnalist.qseq }</td>
					<td><a
						href="ApplixServlet?command=qna_detail_list&qseq=${qnalist.qseq }">${qnalist.subject }</a></td>
					<td>${qnalist.qname }</td>
					<td><fmt:formatDate value="${qnalist.indate }" type="date" /></td>
					<c:choose>
						<c:when test="${qnalist.replyyn=='y' }">
							<td>완료</td>
						</c:when>
						<c:otherwise>
							<td>미완료</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</table>
		<a href="ApplixServlet?command=write_qna_form"><input
			type="button" value="글쓰기" class="button"></a>
	</form>
</nav>
<jsp:include page="/footer.jsp" />