<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<jsp:include page="/header.jsp" />
<link href="../main.css" rel="stylesheet">
<script>
	function inform() {
		alert("메인으로 돌아갑니다");
	}
	
	function update_mypage(){
		document.formm.action = "ApplixServlet?command=update_mypage";
	}
	
</script>
<section>
	<span><%@ include file="sub_menu.jsp"%>
		<nav id="mainbody">
			<form name="formm" method="post">
				<p />
				<fieldset>
					<legend id="malegend">MY PAGE</legend>
					<table id="mypage_table">
						<tr>
							<th>아이디</th>
							<td>${memberVO.id }</td>
						</tr>
						<tr>
							<th>이름</th>
							<td>${memberVO.cname}</td>
						</tr>
						<tr>
							<th>비밀번호</th>
							<td><a href="ApplixServlet?command=change_password_form"><input
									type="button" value="변경" class="button2" style='margin: 10px;'></a></td>
						</tr>
						<tr>
							<th>구매내역</th>
							<td><a href="ApplixServlet?command=orderlist"><input type="button" value="내역 보기"
									class="button2" style='margin: 10px;'></a></td>
						</tr>
						<tr>
							<th>내 글 보기</th>
							<td><a href="ApplixServlet?command=myreview"><input
									type="button" value="리뷰 보기" class="button2"
									style='margin: 10px;'></a><a href="ApplixServlet?command=myboard"><input
									type="button" value="자유게시판 보기" class="button2"
									style='margin: 10px;'></a></td>
						</tr>
						<tr>
							<th>생년월일</th>
							<td>${memberVO.birth}</td>
						</tr>
						<tr>
							<th>본인 확인 이메일</th>
							<td><input type="text" size=35px name="email"></td>
						</tr>
						<tr>
							<th>휴대전화</th>
							<td><input type="text" size=35px name="phone"></td>
						</tr>
					</table>
					<div id="button_id">
						<input type="submit" value="수정" name="join" class="button" onclick="update_mypage()">
						<a href="ApplixServlet?command=index"><input type="button"
							value="취소" name="cancel" class="button" onclick="inform()"></a>
						<a href="ApplixServlet?command=remove_account"><input
							type="button" value="회원탈퇴" name="remove" class="button"></a>
					</div>
				</fieldset>
			</form>
</section>
</nav>
</span>
<jsp:include page="/footer.jsp" />