<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.review_h1 {
	font-size: 300%;
}

#review_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#review_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#review_title:hover {
	text-decoration: underline;
}

#reviewList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

.buuu {
	margin-top: 20px;
	height: 30px;
	width: 120px;
	border-collapse: collapse;
	border: 1px solid #666;
	box-shadow: 3px 3px 2px #ccc;
	font-size: small;
	background-repeat: repeat-x;
	background-position: center center;
	color: #fff;
	margin-right: 10px;
	text-align: center;
	background-color: #CC3D3D;
	font-weight: bold;
	display: inline;
	float: right;
}
}
</style>

<script>
	function go_delete() {
		var count = 0;
		if (document.formm.rseq.length == undefined) {
			if (document.formm.rseq.checked == true) {
				count++;
			}
		}
		for (var i = 0; i < document.formm.rseq.length; i++) {
			if (document.formm.rseq[i].checked == true) {
				count++;
			}
		}

		if (count == 0) {
			alert("삭제할 항목을 선택해주세요.");
		} else {
			document.formm.action = "ApplixServlet?command=delete_post";
			document.formm.submit();
		}
	}
</script>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="review_table">
			<tr>
				<td><h1 class="review_h1">MY REVIEW</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="reviewList" width="100%">
			<tr>
				<th width=50%>제 목</th>
				<th width=20%>작성일</th>
				<th width=10%>삭제하기</th>
			</tr>
			<c:forEach items="${reviewlist}" var="reviewList">
				<tr>
					<td><a
						href="ApplixServlet?command=review_detail&rseq=${reviewList.rseq }"
						id="review_title">${reviewList.subject }</a></td>
					<td><fmt:formatDate value="${reviewList.indate }" type="date" /></td>
					<td><input type="checkbox" name="rseq"
						value="${reviewList.rseq}"></td>
				</tr>
			</c:forEach>
		</table>
		<span> <a
			href="ApplixServlet?command=write_form_review&from=mypage"><input
				type="button" value="새 리뷰쓰기" class="buuu"></a> <!-- <a	href="ApplixServlet?command=delete_post"> </a>-->
			<input type="button" value="선택한 글 삭제" class="buuu"
			onclick="go_delete()"></span>
	</form>
</nav>
<jsp:include page="/footer.jsp" />