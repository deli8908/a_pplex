<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.board_h1 {
	font-size: 300%;
}

#board_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#board_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#board_title:hover {
	text-decoration: underline;
}

#boardList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

option {
	text-align: center;
}
</style>
<script>
	function insert_review() {

		if (document.formm.movieTitle.value == "no") {
			alert("영화 제목을 선택해주세요.");
		} else {
			document.formm.action = "ApplixServlet?command=review_write&name="
					+ "${loginUser.cname}" + "&from=${from}";
			document.formm.submit();
		}
	}
</script>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="board_table">
			<tr>
				<td><h1 class="board_h1">REVIEW</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="boardList">
			<tr>
				<th>영화선택</th>
				<td><select name="movieTitle">
						<option value="no">======== 영화 선택 ========</option>
						<c:forEach items="${movieList}" var="movieList">
							<option value="${movieList.title }">${movieList.title }</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<th>제 목</th>
				<td><input type="text" size="80" name="subject"></td>
			</tr>
			<tr>
				<th>작성자</th>
				<td>${loginUser.cname }</td>
			</tr>
			<tr>
				<th>글내용</th>
				<td><textarea rows="20" cols="80" name="content"></textarea></td>
			</tr>
		</table>
		<center>
			<input type="submit" value="등록하기" class="button2"
				onclick="insert_review()">
		</center>
	</form>
</nav>
<jsp:include page="/footer.jsp" />