<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.qna_h1 {
	font-size: 300%;
}

#qna_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#qna_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#qnaList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

#daedat {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

#qnaList th {
	background-color: #757575;
}
</style>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="qna_table">
			<tr>
				<td><h1 class="qna_h1">Q&A</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="qnaList">
			<tr>
				<th>제 목</th>
				<td>${qnalist.subject }</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td>${qnalist.qname }</td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><fmt:formatDate value="${qnalist.indate }" type="date" /></td>
			</tr>
			<tr>
				<th>내 용</th>
				<td>${qnalist.content}</td>
			</tr>
		</table>
		<table id="daedat" width=100%>
			<tr>
				<th width=20%>답 변</th>
				<th width=60%>답변 내용 <!-- <textarea name="content" cols="40" rows="8"></textarea> -->
				</th>
				<th width=20%>작성일</th>
			</tr>
			<c:choose>
				<c:when test="${qnalist.replyyn == 'n'}">
					<tr>
						<td colspan="3"><h3 style="color: red; text-align: center;">등록된
								답변이 없습니다.</h3></td>
					</tr>
				</c:when>
				<c:otherwise>
						<tr>
							<td>관리자</td>
							<td>${qnalist.reply}</td>
							<td>${qnalist.indate2}</td>
						</tr>
				</c:otherwise>
			</c:choose>
		</table>
	</form>
</nav>
<jsp:include page="/footer.jsp" />