<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link href="../main.css" rel="stylesheet">
<style type="text/css">
.order_h1 {
	font-size: 250%;
	text-align: center;
}

#orderAllList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}
</style>
<article id="mainbody">
	<h1 class="order_h1">Order List</h1>
	<form name="formm" method="post">
		<table id="orderAllList">
			<tr>
				<th>주문일자</th>
				<th>주문번호</th>
				<th>상품명</th>
				<th>결제 금액</th>
				<th>영화보러가기</th>
			</tr>

<c:forEach items="${orderList }" var="orderList">
			<tr>
				<td><fmt:formatDate value="${orderList.indate }" type="date"/></td>
				<td>${orderList.oseq }</td>
				<td>${orderList.title }</td>
				<td><fmt:formatNumber value="${orderList.price }" type="currency"/></td>
				<td><a href="ApplixServlet?command=watch&title=${orderList.title }">보기</a></td>
			</tr>
</c:forEach>

			<%-- 	<c:forEach items="${orderList }" var="orderVO">
				<tr>
					<td><fmt:formatDate value="${orderVO.indate }" type="date" /></td>
					<td>${orderVO.oseq }</td>
					<td>${orderVO.pname}</td>
					<td><fmt:formatNumber value="${orderVO.price2 }"
							type="currency" /></td>
					<td><a
						href="NonageServlet?command/order_detail&oseq=${orderVO.oseq }"
						id="lili">조회</a></td>
				</tr>
			</c:forEach> --%>
		</table>
		<div class="clear"></div>
		<div id="buttons" style="float: right">
			<a href="ApplixServlet?command=index"><input type="button" value="메인으로" class="button"></a>
		</div>
	</form>
</article>
<jsp:include page="/footer.jsp" />