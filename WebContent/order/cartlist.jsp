<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link href="../main.css" rel="stylesheet">
<style type="text/css">
.cart_h1 {
	font-size: 250%;
	text-align: center;
}

#cartList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

label {
	float: right;
}
</style>
<script>
	function go_buy() {
		var count = 0;
		if (document.formm.cseq.length == undefined) {
			if (document.formm.cseq.checked == true) {
				count++;
			}
		}
		for (var i = 0; i < document.formm.cseq.length; i++) {
			if (document.formm.cseq[i].checked == true) {
				count++;
			}
		}
		if (count == 0) {
			alert("주문할 항목을 선택해주세요.");
		} else {
			document.formm.action = "ApplixServlet?command=buy_contents";
			document.formm.submit();
		}
	}

	function go_delete() {
		var count = 0;
		if (document.formm.cseq.length == undefined) {
			if (document.formm.cseq.checked == true) {
				count++;
			}
		}
		for (var i = 0; i < document.formm.cseq.length; i++) {
			if (document.formm.cseq[i].checked == true) {
				count++;
			}
		}
		if (count == 0) {
			alert("삭제할 항목을 선택해주세요.");
		} else {
			document.formm.action = "ApplixServlet?command=cart_delete";
			document.formm.submit();
		}
	}
</script>

<article id="mainbody">
	<h1 class="cart_h1">장바구니</h1>
	<form name="formm" method="post">
		<table id="cartList">
			<tr>
				<th>타이틀</th>
				<th>주문자명</th>
				<th>가 격</th>
				<th>주문일</th>
				<th>선 택</th>
			</tr>

			<c:choose>
				<c:when test="${cartList.size() == 0}">
					<tr>
						<td colspan="5"><h3 style="color: red; text-align: center;">장바구니가
								비었습니다.</h3></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach items="${cartList }" var="cartList">
						<tr>
							<td><a
								href="ApplixServlet?command=contents_detail&num=${cartList.num }">${cartList.title }</a></td>
							<td>${cartList.name }</td>
							<td><fmt:formatNumber value="${cartList.price }"
									type="currency" /></td>
							<td><fmt:formatDate value="${cartList.indate }" type="date" /></td>
							<td><input type="checkbox" name="cseq"
								value="${cartList.cseq }"></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<%-- 				<c:forEach items="${cartList}" var="cartVO">
						<tr>
							<td><a
								href="NonageServlet?command=product_detail&pseq=${cartVO.pseq}">
									<h3>${cartVO.pname}</h3>
							</a></td>
							<td>${cartVO.quantity}</td>
							<td><fmt:formatNumber
									value="${cartVO.price2*cartVO.quantity}" type="currency" /></td>
							<td><fmt:formatDate value="${cartVO.indate}" type="date" /></td>
							<td><input type="checkbox" name="cseq"
								value="${cartVO.cseq}"></td>
						</tr>
					</c:forEach> --%>
		</table>
		<label> 총 주문 금액 : <fmt:formatNumber value="${totalprice }"
				type="currency" /></label>
		<div class="clear"></div>
		<div id="buttons" style="float: right">
			<input type="submit" value="주문하기" class="button" onclick="go_buy()">
			<input type="submit" value="장바구니에서 삭제" class="button"
				onclick="go_delete()"> <a
				href="ApplixServlet?command=allthelist&cate=all"><input
				type="button" value="쇼핑 계속하기" class="button"></a>
		</div>
	</form>
</article>
<jsp:include page="/footer.jsp" />