<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.board_h1 {
	font-size: 300%;
}

#board_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#board_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#board_title:hover {
	text-decoration: underline;
}

#boardList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}
</style>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="board_table">
			<tr>
				<td><h1 class="board_h1">FREE BOARD</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="boardList">
			<tr>
				<th>글번호</th>
				<th>제 목</th>
				<th>아이디</th>
				<th>작성자</th>
				<th>작성일</th>
			</tr>
			<c:forEach items="${freeboard }" var="fbList">
				<tr>
					<td>${fbList.fseq }</td>
					<td><a
						href="ApplixServlet?command=freeboard_detail&fseq=${fbList.fseq }"
						id="board_title">${fbList.subject }</a></td>
					<td >${fbList.id}</td>
					<td>${fbList.fname}</td>
					<td><fmt:formatDate value="${fbList.indate }" type="date" /></td>
				</tr>
			</c:forEach>
		</table>
		<a href="ApplixServlet?command=write_form_freeboard"><input
			type="button" value="글쓰기" class="butem"></a>
	</form>
</nav>
<jsp:include page="/footer.jsp" />