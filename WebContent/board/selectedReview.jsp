<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.review_h1 {
	font-size: 300%;
}

#review_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#review_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#review_title:hover {
	text-decoration: underline;
}

#reviewList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}
</style>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="review_table">
			<tr>
				<td><h1 class="review_h1">${title } 리뷰</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="reviewList">
			<tr>
				<th>글번호</th>
				<th>제 목</th>
				<th>작성자</th>
				<th>작성일</th>
			</tr>
			<c:forEach items="${ review}" var="reviewList">
				<tr>
					<td>${reviewList.rseq }</td>
					<td><a
						href="ApplixServlet?command=review_detail&rseq=${reviewList.rseq }"
						id="review_title">${reviewList.subject }</a></td>
					<td>${reviewList.rname }</td>
					<td><fmt:formatDate value="${reviewList.indate }" type="date" /></td>
				</tr>
			</c:forEach>
		</table>
		<a href="ApplixServlet?command=contents_detail&num=${num }"><input type="button"
			value="이전 화면으로" class="butem"></a>
	</form>
</nav>
<jsp:include page="/footer.jsp" />