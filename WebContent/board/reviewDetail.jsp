<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<link href="../main.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
.review_h1 {
	font-size: 300%;
}

#review_table {
	margin: auto;
	border: 1;
	width: 100%;
}

#review_title {
	text-decoration: none;
	color: FF2D7E;
	font-weight: bold;
}

#reviewList {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

#daedat {
	border-collapse: collapse;
	border-top: 2px solid white;
	border-bottom: 1px solid white;
	width: 100%;
	margin-bottom: 20px;
	font-size: small;
	font-family: "나눔바른고딕";
}

#reviewList th {
	background-color: #757575;
}
</style>
<script>
	function test() {
		document.formm.action = "ApplixServlet?command=review_reply_insert&rseq="
				+ "${rseq}"
				+ "&content="
				+ document.formm.content.value
				+ "&name=" + "${loginUser.cname}";
		document.formm.submit();
	}
	
	function delete_reply(seq, rseq) {
		document.formm.action = "ApplixServlet?command=delete_reply&seq="+seq+"&rseq="+rseq;
		document.formm.submit();
	}
	
</script>
<jsp:include page="/header.jsp" />
<%@ include file="sub_menu.jsp"%>
<nav id="mainbody">
	<form name="formm" method="post">
		<table id="review_table">
			<tr>
				<td><h1 class="review_h1">REVIEW</h1>
				<td>
			</tr>
		</table>
		<table border=1 id="reviewList">
			<tr>
				<th>영화 제목</th>
				<td>${detailList.title }</td>
			</tr>
			<tr>
				<th>제 목</th>
				<td>${detailList.subject }</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td>${detailList.rname }</td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><fmt:formatDate value="${detailList.indate }" type="date" /></td>
			</tr>
			<tr>
				<th>내 용</th>
				<td>${detailList.content }</td>
			</tr>
		</table>
		<label id="datgul">댓글 달기</label><br />
		<table width="100%" id="dat">
			<tr>
				<td><input type="hidden" name="id" value="${loginUser.id}">${loginUser.cname}</td>
				<td><input type="text" size="50" name="content"></td>
				<td><input type="submit" value="등록" class="but"
					onclick="test()"></td>
			</tr>
		</table>
		<br />
		<table id="daedat" width=100%>
			<c:choose>
				<c:when test="${datList.size() == 0}">
					<tr>
						<th width=20%>작성자</th>
						<th width=60%>댓 글</th>
						<th width=20%>작성일</th>
					</tr>
					<tr>
						<td colspan="3"><h3 style="color: red; text-align: center;">등록된
								댓글이 없습니다.</h3></td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<th width=10%>작성자</th>
						<th width=66%>댓 글</th>
						<th width=20%>작성일</th>
						<th width=4%>삭제</th>
					</tr>
					<c:forEach items="${ datList}" var="datList">
						<tr>
							<td>${datList.r2name }</td>
							<td>${datList.content}</td>
							<td><fmt:formatDate value="${datList.indate}" type="date" /></td>
							<c:if test="${datList.id eq loginUser.id}">
								<td><img src="images/del.png"
									style='height: auto; width: 50%;' onclick="delete_reply(${datList.seq},${datList.seqnum })"></td>
							</c:if>
							<c:if test="${datList.id ne loginUser.id}">
								<td>&nbsp;</td>
							</c:if>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</table>
	</form>
</nav>
<jsp:include page="/footer.jsp" />