<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/header.jsp" />
<link href="../main.css"
	rel="stylesheet">
<section>
	<form name="formm" method="post" action="ApplixServlet?command=login">
		<h1 class="login_h1">LOGIN</h1>
		<p />
		<center>
			<table id="login_table" border=2px width=100%>
				<tr>
					<th width=30%>아이디</th>
					<td><input type="text" size=35px name="id"
						style='width: 100%; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th width=30%>비밀번호</th>
					<td><input type="password" size=35px name="pw"
						style='width: 100%; height: 50px; padding: 0;'></td>
				</tr>
			</table>
			<input type="submit" value="로그인" name="login" class="button"></a>
			<a href="ApplixServlet?command=index"><input type="button" value="취소" name="cancel" class="button"></a>
		</center>
	</form>
</section>
<jsp:include page="/footer.jsp" />