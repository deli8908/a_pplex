<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<jsp:include page="/header.jsp" />
<link href="../main.css" rel="stylesheet">
<script>
function joinus(){
	var theForm = document.formm;
	theForm.action="ApplixServlet?command=joinus&month="+theForm.month.value;
	theForm.submit();
}
</script>
<section>
	<form name="formm" method="post">
		<center>
		<h1 class="join_h1">회원가입</h1>
		<p />
			<table id="join_table">
				<tr>
					<th>이름</th>
					<td><input type="text" size=35px name="name"
						style='width: 300px; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th>아이디</th>
					<td><input type="text" size=35px name="id"
						style='width: 300px; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th>비밀번호</th>
					<td><input type="password" size=35px name="pw"
						style='width: 300px; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th>비밀번호 확인</th>
					<td><input type="password" size=35px name="pwcheck"
						style='width: 300px; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th>생년월일</th>
					<td><input type="text" name="year" size="10"
						style="width: 90px; height: 40px;"> <select name="month"
						style="width: 90px; height: 45px;">	
							<option>월</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>
							<option>12</option>
					</select>&nbsp;<input type="text" name="day" size="10"
						style="width: 90px; height: 40px;">
						<p /></td>
				</tr>
				<tr>
					<th>본인 확인 이메일</th>
					<td><input type="text" size=35px name="email"
						style='width: 300px; height: 50px; padding: 0;'></td>
				</tr>
				<tr>
					<th>휴대전화</th>
					<td><input type="text" size=35px name="phone"
						style='width: 300px; height: 50px; padding: 0;'></td>
				</tr>
			</table>
			<input type="submit" value="회원가입" name="join" class="button" onclick="joinus()">
			<a href="ApplixServlet?command=index"><input type="button" value="취소" name="cancel" class="button"></a>
		</center>
	</form>
</section>
<jsp:include page="/footer.jsp" />