<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="main.css" rel="stylesheet">
</head>
<body>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<header>
	<nav class="nav1">
		<a href="ApplixServlet?command=index"><h1 id="maintitle">Applix</h1></a>
	</nav>
	<nav class="nav2">
		<c:choose>
			<c:when test="${empty sessionScope.loginUser}">
				<ul id="mainbar">
					<li><a href="ApplixServlet?command=login_form">LOGIN</a> | <a
						href="ApplixServlet?command=joinusform">JOIN</a> | <a
						href="ApplixServlet?command=allthelist&cate=all">MOVIE LIST</a> | <a
						href="ApplixServlet?command=mypage">MY PAGE</a> | <a
						href="ApplixServlet?command=review">BOARD</a>
				</ul>
			</c:when>
			<c:otherwise>
				<ul id="mainbar">
					<li style="color: white; font-weight: bold;">${sessionScope.loginUser.cname}(${sessionScope.loginUser.id})
						님</li>
					<li><a href="ApplixServlet?command=logout">LOGOUT</a> | <a
						href="ApplixServlet?command=allthelist&cate=all">MOVIE LIST</a> | <a
						href="ApplixServlet?command=cart_list">MY CART</a> | <a
						href="ApplixServlet?command=orderlist">MY MOVIE</a> | <a
						href="ApplixServlet?command=mypage">MY PAGE</a> | <a
						href="ApplixServlet?command=review">BOARD</a>
				</ul>
			</c:otherwise>
		</c:choose>
	</nav>
	<div class="clear"></div>
</header>
</html>